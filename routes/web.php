<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', "GuestController@home");

Route::group(['prefix'=>'berita'], function(){
    Route::get('/', 'GuestController@list_berita');
    Route::get('/get/{id_berita}', 'GuestController@detile_berita');
});

Route::get('/list-agenda', 'GuestController@list_agenda');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/uploads/get/{id}', 'Master\FileManagerController@get_foto');
Route::get('/artikel', 'GuestController@index_artikel');
Route::get('/daftar_anggota', 'GuestController@daftar_anggota');
Route::get('/tentang', 'GuestController@index_tentang');
Route::group(['prefix'=>'list_agenda'], function(){
    Route::get('/', 'GuestController@list_agenda');
    Route::get('/detile/{id_agenda}', 'GuestController@detile_agenda');
});

Route::post('/registrasi_anggota', 'GuestController@registrasi_anggota');

Route::group(['prefix'=>'admin', 'middleware'=>['role:admin']], function(){
    // Routing Anggota
    Route::get('/anggota', 'Master\AnggotaController@index');
    Route::get('/anggota/tambah', 'Master\AnggotaController@form_tambah');
    Route::post('/anggota/tambah', 'Master\AnggotaController@simpan_anggota');
    Route::get('/uploads/anggota/{id}', 'Master\AnggotaController@get_foto');
    Route::get('/anggota/view/{id_anggota}', 'Master\AnggotaController@view_anggota');
    Route::post('/anggota/konfirmasi/{id_anggota}', 'Master\AnggotaController@view_anggota');
    Route::get('/anggota/konfirmasi/{id_anggota}', 'Master\AnggotaController@konfirmasi_form');
    Route::get('/anggota/hapus/{id_anggota}', 'Master\AnggotaController@hapus_anggota');
    Route::get('/anggota/update/{id_anggota}', 'Master\AnggotaController@update_form');
    Route::post('/anggota/update', 'Master\AnggotaController@update_anggota');

    //Routing Jenis Kegiatan
    Route::get('/jenis_kegiatan', 'Master\JenisKegiatanController@index');
    Route::get('/jenis_kegiatan/tambah', 'Master\JenisKegiatanController@tambah_form');
    Route::post('/jenis_kegiatan/simpan', 'Master\JenisKegiatanController@simpan_jenis');
    Route::get('/jenis_kegiatan/update/{id_jenis}', 'Master\JenisKegiatanController@update_form');
    Route::get('/jenis_kegiatan/hapus/{id_jenis}', 'Master\JenisKegiatanController@hapus_jenis');
    Route::post('/jenis_kegiatan/update', 'Master\JenisKegiatanController@update_jenis');
    Route::get('/jenis_kegiatan/view/{id_jenis}', 'Master\JenisKegiatanController@view_jenis_kegiatan');
    Route::get('/jenis_kegiatan/tambah_anggota/{jenis_kegiatan_id}', 'Master\JenisKegiatanController@tambah_anggota');
    Route::post('/jenis_kegiatan/simpan_anggota', 'Master\JenisKegiatanController@simpan_anggota');
    Route::get('/jenis_kegiatan/view/unlink_anggota/{id_jenis}/{id_anggota}', 'Master\JenisKegiatanController@unlink_anggota');

    //Routing Penempatan Anggota
    Route::get('/penempatan', 'Master\PenempatanAnggotaController@index');
    Route::get('/get_data_jenis', 'Master\PenempatanAnggotaController@get_data_jenis');

    //Routing Role
    Route::get('/preferences/role', 'Master\PreferencesController@index_role');
    Route::get('/preferences/role/tambah', 'Master\PreferencesController@tambah_role_form');
    Route::post('/preferences/role/simpan', 'Master\PreferencesController@simpan_role');
    Route::get('/preferences/role/hapus/{id_role}', 'Master\PreferencesController@hapus_role');

    //Routing Hak Akses
    Route::get('/preferences/hakakses', 'Master\PreferencesController@index_hakakses');
    Route::get('/preferences/hakakses/tambah', 'Master\PreferencesController@form_tambah_hakakses');
    Route::post('/preferences/hakakses/simpan', 'Master\PreferencesController@simpan_hakakses');
    Route::get('/preferences/hakakses/hapus_permission/{id_role}/{id_permission}', 'Master\PreferencesController@hapus_permission');
    Route::get('/preferences/hakakses/unlink/{id_role}/{id_permission}', 'Master\PreferencesController@unlink_permission');
    Route::get('/preferences/hakakses/set_permission/{id_role}', 'Master\PreferencesController@set_permission_form');
    Route::post('/preferences/hakakses/set_permission', 'Master\PreferencesController@set_permission');

    //Routing User Manajemen
    Route::get('/preferences/usermanajemen','Master\PreferencesController@index_usermanajemen');

    //Posting Kegiatan
    Route::get('/preferences', 'Master\PreferencesController@index');

    //Keuangan Routes
    Route::get('/keuangan', 'Master\KeuanganController@index_keuangan');
    Route::get('/keuangan/tambah_pemasukan', 'Master\KeuanganController@tambah_pemasukan');
    Route::get('/keuangan/tambah_pengeluaran', 'Master\KeuanganController@tambah_pengeluaran');
    Route::get('/keuangan/hapus/{id_keuangan}', 'Master\KeuanganController@hapus_keuangan');
    Route::post('/keuangan/simpan', 'Master\KeuanganController@simpan');

    //Route Laporan Keuangan
    Route::get('/laporan_keuangan', 'Master\KeuanganController@index_laporan');

    //Route Pendaftaran
    Route::group(['prefix'=>'pendaftaran'], function(){
        Route::get('/', 'Master\AnggotaController@index_pendaftaran');
        Route::get('/konfirmasi/{id_calon}', 'Master\AnggotaController@konfirmasi_calon');
        Route::get('/detile/{id_calon}', 'Master\AnggotaController@detile_calon');
        Route::get('/uploads/{id_calon}', 'Master\AnggotaController@get_foto_calon');
    });
});

Route::group(['middleware'=>['role:admin|user']], function(){
    Route::get('/posting', 'Master\PostinganController@index_posting');
    Route::get('/posting/baru', 'Master\PostinganController@posting_baru_form');
    Route::post('/posting/simpan', 'Master\PostinganController@simpan_posting');
    Route::get('/posting/hapus/{id_posting}', 'Master\PostinganController@hapus_posting');
    Route::get('/posting/edit/{id_posting}', 'Master\PostinganController@edit_posting');
    Route::post('/posting/edit/simpan', 'Master\PostinganController@simpan_perubahan');
    Route::get('/posting/preview/{id_posting}', 'Master\PostinganController@preview_posting');

    //Route Agenda
    Route::get('/manage/agenda', 'AgendaController@index_agenda');
    Route::get('/manage/agenda/baru', 'AgendaController@tambah_agenda');
    Route::post('/manage/agenda/simpan', 'AgendaController@simpan_agenda');
    Route::get('/manage/agenda/detile/{id_agenda}', 'AgendaController@detile_agenda');
    Route::get('/manage/agenda/ubah/{id_agenda}', 'AgendaController@ubah_form');
    Route::post('/manage/agenda/simpan_perubahan', 'AgendaController@simpan_perubahan');
    Route::get('/manage/agenda/hapus/{id_agenda}', 'AgendaController@hapus');





});
