<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserJenisKegiatansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_jenis_kegiatan', function (Blueprint $table) {
            $table->integer('anggota_id')->unsigned();
            $table->integer('jenis_kegiatan_id')->unsigned();

            $table->foreign('anggota_id')->references('id')->on('anggota')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('jenis_kegiatan_id')->references('id')->on('jenis_kegiatan')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->primary(['anggota_id', 'jenis_kegiatan_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_jenis_kegiatan');
    }
}
