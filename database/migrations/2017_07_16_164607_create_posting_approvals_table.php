<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostingApprovalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posting_approval', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_posting')->unsigned();
            $table->enum('status', ['waiting', 'approval', 'reject']);
            $table->foreign('id_posting')->references('id')->on('postings')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posting_approval');
    }
}
