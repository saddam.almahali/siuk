<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCalonJurusanJenisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('calon_jurusan_jenis', function (Blueprint $table) {
            $table->integer('id_calon')->unsigned();
            $table->integer('id_jenis')->unsigned();
            $table->foreign('id_calon')->references('id')->on('calon_anggota')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('id_jenis')->references('id')->on('jenis_kegiatan')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('calon_jurusan_jenis');
    }
}
