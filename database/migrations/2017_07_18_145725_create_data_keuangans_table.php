<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDataKeuangansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_keuangan', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_anggota');
            $table->date('tanggal');
            $table->double('jumlah',20);
            $table->double('total', 20);
            $table->text('keterangan');
            $table->enum('tipe', ['masuk', 'keluar']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_keuangan');
    }
}
