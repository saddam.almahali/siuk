<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCalonAnggotasTable extends Migration
{

    public function up()
    {
        Schema::create('calon_anggota', function (Blueprint $table) {
            $table->increments('id');
            $table->string('npm')->unique();
            $table->string('nama_lengkap');
            $table->date('tanggal_daftar');
            $table->string('tempat_lahir')->length(50);
            $table->enum('jenis_kelamin', ['l', 'p']);
            $table->date('tanggal_lahir');
            $table->text('alamat_asal');
            $table->text('email')->unique();
            $table->text('alamat_kos')->nullable();
            $table->string('telp')->length(20)->nullable();
            $table->string('facebook')->nullable();
            $table->string('twitter')->nullable();
            $table->string('bbm')->length(15)->nullable();
            $table->string('hobi')->length(255)->nullable();
            $table->string('moto')->length(255)->nullable();
            $table->string('harapan')->length(255)->nullable();
            $table->string('foto')->length(255)->nullable();
            $table->enum('status', ['bk', 'k']);

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('calon_anggota');
    }
}
