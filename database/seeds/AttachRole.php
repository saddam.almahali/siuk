<?php

use Illuminate\Database\Seeder;
use App\Permission;
use App\User;
use App\Role;
class AttachRole extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $createPost = new Permission();
        $createPost->name         = 'create-post';
        $createPost->display_name = 'Create Posts'; // optional
// Allow a user to...
        $createPost->description  = 'create new blog posts'; // optional
        $createPost->save();

        $admin = Role::find(1);
        $user = User::find(1);

        $user->attachRole($admin);
        $admin->attachPermission($createPost);

    }
}
