<?php

use Illuminate\Database\Seeder;
use App\User;

class UserSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin_user = new User();
        $admin_user->name = 'Admin';
        $admin_user->username = 'admin';
        $admin_user->email = 'admin@test.com';
        $admin_user->password = bcrypt('123456');
        $admin_user->save();
    }
}
