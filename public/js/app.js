/**
 * Created by saddam on 09/07/2017.
 */
var form_tambah = new Vue({
    el: '#form-tambah',
    data: {
        pesan : "Hello Ini Pesan Dari Vue",
        picValue: "",
        filefoto: ""
    },
    methods: {
        pilih_gambah: function(e){
            console.log(e.target.files);
            readURL(e.target);
        }
    }

});


function readURL(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#foto_profil').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}