@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-9 blog-info">
                <section class="blog-single">
                    <div class="blog-slide">
                        <ul class="slides">
                            <li>
                                <div class="img-responsive">
                                    <img data-src="{{url('/uploads/get').'/'.$posting->gambar()->first()->gambar}}" alt="blog-image 1" class="img img-thumbnail b-lazy" width="666" height="300" />
                                </div>

                            </li>
                        </ul>
                    </div>

                    <div class="single-post">
                        <h4 class="services-title-one subtitle">{{strtoupper($posting->judul_posting)}}</h4>
                        {!! $posting->isi_posting !!}

                    </div>

                    <div class="social-share text-center">
                        <a class="fb-share" href="#"> <i class="fa fa-facebook" aria-hidden="true"></i>Share on Facebook </a>
                        <a class="tweet-share" href="#"> <i class="fa fa-twitter" aria-hidden="true"></i>Share on Twitter </a>
                    </div>



                </section>
            </div>

            <div class="col-md-3 sidebar">

                <div class="login-box">
                    <button class="btn btn-info btn-block" onclick="location.href='/login'"><i class="fa fa-key"></i>&nbsp;&nbsp;LOGIN</button>
                    <button class="btn btn-success btn-block" onclick="location.href='/daftar_anggota'"><i class="fa fa-calendar-o"></i>&nbsp;&nbsp;DAFTAR</button>

                </div>
                <br />
                <div class="plubication-downloads">
                    <h2 class="publish"><i class="fa fa-calendar"></i>&nbsp;&nbsp;AGENDA</h2>

                    <ul class="download-list">
                        @forelse($data_agenda as $agenda)
                            <li><a href="{{url('/manage/agenda/detile').'/'.$agenda->id}}"> {{$agenda->nama_kegiatan}} </a> <span>{{date('d-m-Y', strtotime($agenda->tanggal))}}</span></li>
                        @empty
                            <li class="text-center">Tidak Terdapat Anggota</li>
                        @endforelse
                    </ul>
                </div>

            </div>

        </div>
    </div>
@endsection

@section('style')
    <link href="{{url('css/jquery.growl.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{url('css/bootstrap-datepicker3.min.css')}}" rel="stylesheet" type="text/css" />
    <style>
        .b-lazy {
            opacity:0;
            transform: scale(3);
            transition: all 500ms;
        }
        .b-loaded {
            opacity:1;
            transform: scale(1);
        }
        span.month{
            color: #8d8d8d;
        }
        .konten {
            margin-top: 20px;
        }
        .konten > .row {
            margin-right: 20px;
            margin-left: 30px;
        }
        .row{
            margin-top: 10px;
            margin-bottom: 10px;
        }
        .form-group{
            margin-right: 20px;
        }
    </style>
@endsection
@section('script')
    <script src="{{url('/js/blazy.min.js')}}" type="text/javascript"></script>
    <script src="{{url('js/jquery.growl.js')}}" type="text/javascript"></script>
    <script src="{{url('js/bootstrap-datepicker.min.js')}}" type="text/javascript"></script>

    <script>
                @if(request()->session()->has('sukses'))
        var msg = "{{request()->session()->get('sukses')}}";
        $.growl.notice({ title:"Sukses!", message: msg });
                @endif

                @if(request()->session()->has('gagal'))
        var msg = "{{request()->session()->get('gagal')}}";
        $.growl.error({ title:"Gagal!", message: msg });
                @endif
        var option = {
                clearBtn: true,
                autoclose: true,
                format: 'dd-mm-yyyy'
            };
        $('#dari_tanggal').datepicker(option);
        $('#sampai_tanggal').datepicker(option);
    </script>
    <script>
        var bLazy = new Blazy({
            breakpoints: [{
                width: 420 // Max-width
            }]
            , success: function(element){
                setTimeout(function(){
                    // We want to remove the loader gif now.
                    // First we find the parent container
                    // then we remove the "loading" class which holds the loader image

                }, 200);
            }
        });
    </script>
@endsection