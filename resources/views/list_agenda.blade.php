@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">

            <div class="col-md-9 blog-info">
                <div class="box-agenda">
                    <div class="page-header">
                        <h2>DAFTAR AGENDA UKM SENI</h2>
                    </div>
                    @if($data_agenda->count() > 0)
                        <ol>
                            @foreach($data_agenda as $agenda)
                            <li><a href="{{url('/manage/agenda/detile').'/'.$agenda->id}}"> {{$agenda->nama_kegiatan}} </a> <span>{{date('d-m-Y', strtotime($agenda->tanggal))}}</span></li>
                            @endforeach
                        </ol>
                    @else
                        <strong>Tidak Ada Agenda</strong>
                    @endif
                </div>

            </div>

            <div class="col-md-3 sidebar">
                <div class="login-box">
                    <button class="btn btn-info btn-block" onclick="location.href='/login'"><i class="fa fa-key"></i>&nbsp;&nbsp;LOGIN</button>
                    <button class="btn btn-success btn-block" onclick="location.href='/daftar_anggota'"><i class="fa fa-calendar-o"></i>&nbsp;&nbsp;DAFTAR</button>

                </div>
            </div>

        </div>
    </div>
@endsection

@section('style')
    <style>
        .b-lazy {
            opacity:0;
            transform: scale(3);
            transition: all 500ms;
        }
        .b-loaded {
            opacity:1;
            transform: scale(1);
        }
        #widget-user{

            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
            margin-top: 10px;
            margin-left: 10px;
            padding: 10px;
        }

        .box-agenda {
            margin-top: 20px;
            margin-right: 50px;
            margin-left: 50px;
        }

        #widget-user.img {
            width: 100px;
        }

        .panel {
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
            margin-top: 10px;
        }

    </style>
@endsection
@section('script')
    <script src="{{url('/js/blazy.min.js')}}" type="text/javascript"></script>
    <script>
        var bLazy = new Blazy({
            breakpoints: [{
                width: 420 // Max-width
            }]
            , success: function(element){
                setTimeout(function(){
                    // We want to remove the loader gif now.
                    // First we find the parent container
                    // then we remove the "loading" class which holds the loader image

                }, 200);
            }
        });
    </script>
@endsection