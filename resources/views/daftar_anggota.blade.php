@extends('layouts.app')
@section('content')
    <div class="konten">
        <div class="row">
            <div class="col-md-8">
                <div class="register-box">
                    <div class="page-header">
                        <h3>FORM REGISTRASI ANGGOTA BARU</h3>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            @if(session()->has('sukses'))
                                <div class="alert alert-success" role="alert"><strong>Berhasil! </strong>{{session()->get('sukses')}}</div>
                            @endif
                        </div>
                    </div>
                    <form action="{{url('/registrasi_anggota')}}" method="POST" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="form-group {{$errors->has('npm') ? 'has-error' : ''}}">
                            <label for="npm">NPM</label>
                            <input type="text" class="form-control input-lg" name="npm" value="{{old('npm')}}" placeholder="Masukan Nama Lengkap">
                            @if($errors->has('npm'))
                                <span class="help-block">
                                    <strong>{{$errors->first('npm')}}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group {{$errors->has('nama_lengkap') ? 'has-error' : ''}}">
                            <label for="nama_lengkap">Nama Lengkap</label>
                            <input type="text" class="form-control input-lg" name="nama_lengkap" value="{{old('nama_lengkap')}}" placeholder="Masukan Nama Lengkap">
                            @if($errors->has('nama_lengkap'))
                                <span class="help-block">
                                    <strong>{{$errors->first('nama_lengkap')}}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group {{$errors->has('jenis_kelamin') ? 'has-error' : ''}}">
                            <label for="jenis_kelamin">Jenis Kelamin</label>
                            <select name="jenis_kelamin" id="jenis_kelamin" class="form-control input-lg">
                                <option value="" disabled selected>Pilih Jenis Kelamin</option>
                                <option value="l">Laki-Laki</option>
                                <option value="p">Perempuan</option>
                            </select>
                            @if($errors->has('jenis_kelamin'))
                                <span class="help-block">
                                    <strong>{{$errors->first('jenis_kelamin')}}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group {{$errors->has('tempat_lahir') ? 'has-error' : ''}}">
                            <label for="tempat_lahir">Tempat Lahir</label>
                            <input type="text" name="tempat_lahir" class="form-control input-lg" value="{{old('tempat_lahir')}}" placeholder="Masukan Tempat Lahir">
                            @if($errors->has('tempat_lahir'))
                                <span class="help-block">
                                    <strong>{{$errors->first('tempat_lahir')}}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group {{$errors->has('tanggal_lahir') ? 'has-error' : ''}}">
                            <label for="tanggal_lahir">Tanggal Lahir</label>
                            <input type="text" name="tanggal_lahir" id="tanggal_lahir" class="form-control input-lg" value="{{old('tempat_lahir')}}" placeholder="Tentukan Tanggal Lahir">
                            @if($errors->has('tanggal_lahir'))
                                <span class="help-block">
                                    <strong>{{$errors->first('tanggal_lahir')}}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group {{$errors->has('email') ? 'has-error' : ''}}">
                            <label for="email">Email</label>
                            <input type="text" name="email" class="form-control input-lg" value="{{old('tempat_lahir')}}" placeholder="Masukan Email Mahasiswa">
                            @if($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{$errors->first('email')}}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group {{$errors->has('alamat_asal') ? 'has-error' : ''}}">
                            <label for="alamat_asal">Alamat Asal</label>
                            <textarea name="alamat_asal" id="alamat_asal" rows="2" class="form-control input-lg" placeholder="Masukan Alamat Asal">{{old('alamat_asal')}}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="alamat_kos">Alamat Kos</label>
                            <textarea name="alamat_kos" id="alamat_kos" rows="2" class="form-control input-lg" placeholder="Masukan Alamat Kos"></textarea>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="telp">Telepon/HP</label>
                                    <input type="text" name="telp" id="telp" class="form-control input-lg" placeholder="Masukan Nomor HP/Telp">
                                </div>
                                <div class="form-group">
                                    <label for="facebook">Alamat Facebook</label>
                                    <input type="text" name="facebook" class="form-control input-lg" placeholder="Masukan Alamat Profil FB">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="twitter">Alamat Twitter</label>
                                    <input type="text" name="twitter" id="twitter" class="form-control input-lg" placeholder="Masukan Alamat Twitter">
                                </div>
                                <div class="form-group">
                                    <label for="bbm">PIN BBM</label>
                                    <input type="text" name="bbm" class="form-control input-lg" placeholder="Masukan PIN BBM">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="hobi">Hobi</label>
                            <input type="text" name="hobi" class="form-control input-lg" placeholder="Masukan Hobi (Opsional)">
                        </div>
                        <div class="form-group">
                            <label for="moto">Moto</label>
                            <input type="text" name="moto" class="form-control input-lg" placeholder="Masukan Moto (Opsional)">
                        </div>
                        <div class="form-group">
                            <label for="harapan">Harapan</label>
                            <input type="text" name="harapan" class="form-control input-lg" placeholder="Masukan Harapan (Opsional)">
                        </div>
                        <div class="form-group {{$errors->has('jenis_kegiatan') ? 'has-error' : ''}}">
                            <label for="jenis_kegiatan ">Pilih Jenis Kegiatan yang Akan Diikuti</label>
                            <select name="jenis_kegiatan[]" id="jenis_kegiatan" class="form-control input-lg" multiple>
                                @foreach($data_jenis as $jenis)
                                    <option value="{{$jenis->id}}">{{$jenis->kode_jenis.' | '.$jenis->nama_jenis}}</option>
                                @endforeach
                            </select>
                            @if($errors->has('jenis_kegiatan'))
                                <span class="help-block">
                                    <strong>{{$errors->first('jenis_kegiatan')}}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="foto">Foto</label>
                            <input type="file" name="foto" class="form-control input-lg" >
                        </div>
                        <div class="form-group">
                            <center>
                                <input type="submit" class="btn btn-primary btn-lg" value="Daftar">
                            </center>
                        </div>

                    </form>
                </div>

            </div>
            <div class="col-md-4">
                <div class="plubication-downloads">
                    <h2 class="publish"><i class="fa fa-info"></i>&nbsp;&nbsp;SYARAT PENDAFTARAN</h2>
                    <dif class="row">
                        <div class="col-md-12">
                            <ol class="syarat">
                                <li>Mahasiswa Sekolah Tinggi Teknologi Garut</li>
                                <li>Mempunyai Bakat Dalam Bidang Seni</li>
                                <li>Memiliki Minat di Bidang Seni</li>
                            </ol>
                        </div>
                    </dif>



                </div>
            </div>
        </div>
    </div>
@endsection

@section('style')
    <link rel="stylesheet" href="{{url('/css/bootstrap-datepicker3.min.css')}}">
    <style>
        div.konten{
            margin-top: 20px;
            margin-left: 50px;
            margin-right: 50px;
        }
        span.month{
            color: #8d8d8d;
        }

        .syarat{
            padding-left: 10px;
            font-size: 15px;
        }
    </style>

@endsection

@section('script')
    <script src="{{url('js/bootstrap-datepicker.min.js')}}"></script>
    <script>
        $('#tanggal_lahir').datepicker({
            clearBtn: true,
            autoclose: true,
            format: 'dd-mm-yyyy'
        });
    </script>
@endsection