@extends('layouts.app')

@section('content')
    <div class="konten">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h4 class="panel-title">Set Permission Role</h4>
                    </div>
                    <div class="panel-body">
                        <form action="{{url('/admin/preferences/hakakses/set_permission')}}" method="post">
                            {{csrf_field()}}
                            <div class="form-group {{$errors->has('id_role') ? 'has-error' : ''}}">
                                <label for="name_role">Nama Role</label>
                                <input type="text" id="name_role" class="form-control input-sm" readonly value="{{$role->display_name}}">
                                <input type="hidden" name="id_role" value="{{$role->id}}">
                                @if($errors->has('id_role'))
                                    <span class="help-block">
                                        <strong>{{$errors->first('id_role')}}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group {{$errors->has('id_permission') ? 'has-error' : ''}}">
                                <select class="form-control input-sm" name="id_permission" value="{{old('id_permission')}}">
                                    <option value="">Pilih Permission</option>
                                    @foreach($data_permission as $permission)
                                        <option value="{{$permission->id}}">{{$permission->display_name}}</option>
                                    @endforeach
                                </select>
                                @if($errors->has('id_permission'))
                                    <span class="help-block">
                                        <strong>{{$errors->first('id_permission')}}</strong>
                                    </span>
                                @endif
                            </div>


                            <input type="submit" class="btn btn-primary btn-sm pull-right" style="margin-left:15px; width:100px;" value="Simpan">
                            <input type="submit" class="btn btn-danger btn-sm pull-right" style="width: 100px" onclick="event.preventDefault(); history.back();" value="Batal">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('style')
    <style>
        .konten {
            margin-top: 20px;
        }
        .konten > .row {
            margin-right: 20px;
            margin-left: 30px;
        }
        .row{
            margin-top: 10px;
            margin-bottom: 10px;
        }
    </style>
@endsection

@section('script')

@endsection