@extends('layouts.app')

@section('content')
    <div class="konten">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h4 class="panel-title"><i class="fa fa-key"></i>&nbsp;&nbsp;List Hak Akses</h4>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="pull-right">
                                    <a href="{{url('/admin/preferences/hakakses/tambah')}}" class="btn btn-primary btn-sm">&nbsp;&nbsp;Tambah Permission</a>
                                </div>
                            </div>
                        </div>
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th style="text-align: center; width: 10%;">#</th>
                                <th style="text-align: center; width: 15%;">Nama</th>
                                <th style="text-align: center; width: 15%;">Nama Tampilan</th>
                                <th style="text-align: center;">Deskripsi</th>
                                <th style="text-align: center; width: 15%">Permission</th>
                                <th style="text-align: center; width: 20%;">Opsi</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $i = 0; $roleNow = null; ?>
                            @forelse($data_role as $role)

                                <tr>
                                    <td align="center">{{$i+1}}</td>
                                    <td align="center">{{$role->name}}</td>
                                    <td align="center">{{$role->display_name}}</td>
                                    <td>{{$role->description}}</td>
                                    <td style="padding-left: 20px;">
                                        @if($role->perms->count()>0)
                                            <ol>
                                                @foreach($role->perms as $permission)
                                                    <li>{{$permission->display_name}} &nbsp;&nbsp;
                                                        @if($role->name != 'admin')
                                                            <a href="{{url('/admin/preferences/hakakses/unlink').'/'.$role->id.'/'.$permission->id }}"
                                                               onclick="if(confirm('Apakah Anda Yakin Ingin Menghapus Akses Permission?')){return true;}else return false;"
                                                               class="btn-link" title="Unlink Permission">
                                                                <span class="fa fa-unlink"></span>
                                                            </a>
                                                        @else
                                                            <a href="{{url('/admin/preferences/hakakses/hapus_permission').'/'.$role->id.'/'.$permission->id }}"
                                                               onclick="if(confirm('Apakah Anda Yakin Ingin Menghapus Permission?')){return true;}else return false;"
                                                               class="btn-link" title="Hapus Permission">
                                                                <span class="fa fa-minus"></span>
                                                            </a>
                                                        @endif

                                                    </li>

                                                @endforeach
                                            </ol>
                                            @if($role->name != 'admin')
                                                <a href="{{url('/admin/preferences/hakakses/set_permission').'/'.$role->id}}" class="btn-link"><i class="fa fa-pencil"></i>&nbsp;&nbsp;Set Permission</a>
                                            @endif
                                        @else
                                            @if($role->name != 'admin')
                                                <a href="{{url('/admin/preferences/hakakses/set_permission').'/'.$role->id}}" class="btn-link">Set Permission</a>
                                            @else
                                                Tidak Terdapat Permission
                                            @endif
                                        @endif
                                    </td>
                                    <td class="text-center">
                                        <a href="{{url('/admin/preferences/role/hapus').'/'.$role->id}}" class="btn btn-danger btn-sm" onclick="if(confirm('Apakah anda yakin ingin menghapus data?')){return true;}else return false;">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                    </td>
                                </tr>
                                <?php $i++; ?>
                            @empty
                                <tr>
                                    <td colspan="5">Tidak Ada Data Role</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('style')
    <link href="{{url('css/jquery.growl.css')}}" rel="stylesheet" type="text/css" />
    <style>
        .konten {
            margin-top: 20px;
        }
        .konten > .row {
            margin-right: 20px;
            margin-left: 30px;
        }
        .row{
            margin-top: 10px;
            margin-bottom: 10px;
        }
    </style>
@endsection

@section('script')
    <script src="{{url('js/jquery.growl.js')}}" type="text/javascript"></script>
    <script>
        @if(request()->session()->has('sukses'))
            var msg = "{{request()->session()->get('sukses')}}";
            $.growl.notice({ title:"Sukses!", message: msg });
        @endif

        @if(request()->session()->has('gagal'))
            var msg = "{{request()->session()->get('gagal')}}";
            $.growl.error({ title:"Gagal!", message: msg });
        @endif
    </script>

@endsection