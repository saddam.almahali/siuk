@extends('layouts.app')

@section('content')
    <div class="konten">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <i class="fa fa-user"></i>&nbsp;&nbsp; Preferences
                        </h4>
                    </div>

                    <div class="panel-body">
                        <div>

                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active"><a href="#hakakses" aria-controls="hakakses" role="tab" data-toggle="tab">Hak Akses</a></li>
                                <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Profile</a></li>
                                <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Messages</a></li>
                                <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Settings</a></li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="hakakses">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <table class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th style="text-align: center;">#</th>
                                                        <th style="text-align: center;">Name</th>
                                                        <th style="text-align: center;">Display Name</th>
                                                        <th style="text-align: center;">Description</th>
                                                    </tr>
                                                </thead>
                                                @forelse($data_role as $role)
                                                    <tr>
                                                        <td align="center">
                                                            <input type="checkbox" name="select">
                                                        </td>
                                                        <td align="center">{{$role->name}}</td>
                                                        <td align="center">{{$role->display_name}}</td>
                                                        <td align="center">{{$role->description}}</td>
                                                    </tr>
                                                @empty

                                                @endforelse

                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="profile">...</div>
                                <div role="tabpanel" class="tab-pane" id="messages">...</div>
                                <div role="tabpanel" class="tab-pane" id="settings">...</div>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection

@section('style')
    <style>
        .konten {
            margin-top: 20px;
        }
        .konten > .row {
            margin-right: 20px;
            margin-left: 30px;
        }
        .row{
            margin-top: 10px;
            margin-bottom: 10px;
        }
    </style>
@endsection

@section('script')

@endsection