@extends('layouts.app')

@section('content')
    <div class="konten">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h4 class="panel-title">List Role</h4>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="pull-right">
                                    <a href="{{url('/admin/preferences/role/tambah')}}" class="btn btn-primary btn-sm">Tambah Data</a>
                                </div>
                            </div>
                        </div>
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th style="text-align: center; width: 10%;">#</th>
                                <th style="text-align: center; width: 15%;">Nama</th>
                                <th style="text-align: center; width: 15%;">Nama Tampilan</th>
                                <th style="text-align: center;">Deskripsi</th>
                                <th style="text-align: center; width: 20%;">Opsi</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $i = 0; ?>
                            @forelse($data_role as $role)
                                <tr>
                                    <td align="center">{{$i+1}}</td>
                                    <td align="center">{{$role->name}}</td>
                                    <td align="center">{{$role->display_name}}</td>
                                    <td>{{$role->description}}</td>
                                    <td class="text-center">
                                        <a href="{{url('/admin/preferences/role/hapus').'/'.$role->id}}" class="btn btn-danger btn-sm" onclick="if(confirm('Apakah anda yakin ingin menghapus data?')){return true;}else return false;">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                    </td>
                                </tr>
                                <?php $i++; ?>
                            @empty
                                <tr>
                                    <td colspan="5">Tidak Ada Data Role</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('style')
    <link href="{{url('css/jquery.growl.css')}}" rel="stylesheet" type="text/css" />
    <style>
        .konten {
            margin-top: 20px;
        }
        .konten > .row {
            margin-right: 20px;
            margin-left: 30px;
        }
        .row{
            margin-top: 10px;
            margin-bottom: 10px;
        }
    </style>
@endsection

@section('script')
    <script src="{{url('js/jquery.growl.js')}}" type="text/javascript"></script>
    <script>
        @if(request()->session()->has('sukses'))
            var msg = "{{request()->session()->get('sukses')}}";
            $.growl.notice({ title:"Sukses!", message: msg });
        @endif

        @if(request()->session()->has('gagal'))
            var msg = "{{request()->session()->get('gagal')}}";
            $.growl.error({ title:"Gagal!", message: msg });
        @endif
    </script>

@endsection