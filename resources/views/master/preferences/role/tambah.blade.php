@extends('layouts.app')

@section('content')
    <div class="konten">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h4 class="panel-title">Form Tambah Role</h4>
                    </div>
                    <div class="panel-body">
                        <form action="{{url('/admin/preferences/role/simpan')}}" method="post">
                            {{csrf_field()}}
                            <div class="form-group {{$errors->has('name') ? 'has-error' : ''}}">
                                <input type="text" class="form-control input-sm" name="name" value="{{old('name')}}" placeholder="Nama Role">
                                @if($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{$errors->first('name')}}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group {{$errors->has('display_name') ? 'has-error' : ''}}">
                                <input type="text" class="form-control input-sm" value="{{old('display_name')}}" name="display_name" placeholder="Nama Layar">
                                @if($errors->has('display_name'))
                                    <span class="help-block">
                                        <strong>{{$errors->first('display_name')}}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group {{$errors->has('description') ? 'has-error' : ''}}">
                                <textarea name="description" id="description" class="form-control input-sm" rows="3" placeholder="Keterangan">{{old('keterangan')}}</textarea>
                                @if($errors->has('description'))
                                    <span class="help-block">
                                        <strong>{{$errors->first('description')}}</strong>
                                    </span>
                                @endif
                            </div>

                            <input type="submit" class="btn btn-primary btn-sm pull-right" style="margin-left:15px; width:100px;" value="Simpan">
                            <input type="submit" class="btn btn-danger btn-sm pull-right" style="width: 100px" onclick="event.preventDefault(); history.back();" value="Batal">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('style')
    <style>
        .konten {
            margin-top: 20px;
        }
        .konten > .row {
            margin-right: 20px;
            margin-left: 30px;
        }
        .row{
            margin-top: 10px;
            margin-bottom: 10px;
        }
    </style>
@endsection

@section('script')

@endsection