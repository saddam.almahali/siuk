@extends('layouts.app')

@section('content')
    <div class="konten">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h4 class="panel-title">User Manajemen</h4>
                    </div>
                    <div class="panel-body">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th width="5%" style="text-align: center;">#</th>
                                <th width="20%" style="text-align: center;">Nama</th>
                                <th width="15%" style="text-align: center;">Username</th>
                                <th width="20%" style="text-align: center;">Email</th>
                                <th width="10%" style="text-align: center;">Role</th>
                                <th style="text-align: center;">Opsi</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $i=0; ?>
                            @forelse($data_user as $user)
                                <tr>
                                    <td align="center">{{$i+1}}</td>
                                    <td align="center">{{$user->name}}</td>
                                    <td align="center">{{$user->username}}</td>
                                    <td align="center">{{$user->email}}</td>
                                    <td align="center">{!! $user->roles()->first()->name == 'admin' ? '<span class="label label-success">Admin</span>' : '<span class="label label-info">User</span>' !!}</td>
                                    <td align="center">
                                        @if($user->id != auth()->user()->id)
                                            <a href="#" class="btn btn-info btn-sm" title="Hak Akses"><i class="fa fa-sign-in"></i></a>
                                        @endif
                                    </td>
                                </tr>
                                <?php $i++; ?>
                            @empty
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('style')
    <link href="{{url('css/jquery.growl.css')}}" rel="stylesheet" type="text/css" />
    <style>
        .konten {
            margin-top: 20px;
        }
        .konten > .row {
            margin-right: 20px;
            margin-left: 30px;
        }
        .row{
            margin-top: 10px;
            margin-bottom: 10px;
        }
    </style>
@endsection

@section('script')
    <script src="{{url('js/jquery.growl.js')}}" type="text/javascript"></script>
    <script>
                @if(request()->session()->has('sukses'))
        var msg = "{{request()->session()->get('sukses')}}";
        $.growl.notice({ title:"Sukses!", message: msg });
                @endif

                @if(request()->session()->has('gagal'))
        var msg = "{{request()->session()->get('gagal')}}";
        $.growl.error({ title:"Gagal!", message: msg });
        @endif
    </script>

@endsection