@extends('layouts.app')

@section('content')
    <div class="konten" id="penempatan_board">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h4 class="panel-title"><i class="fa fa-sign-in"></i>&nbsp;&nbsp;Penempatan Keanggotaan</h4>
                    </div>
                    <div class="panel-body">
                        <div class="row">

                            <div class="col-md-3">
                                <div class="list-group">
                                    @forelse($jenis_kegiatan as $jenis)
                                        <a href="#" class="list-group-item">
                                            {{$jenis->nama_jenis}}
                                        </a>
                                    @empty
                                        <a href="#" class="list-group-item" v-for="item in jenis_kegiatan">
                                            Data Kosong
                                        </a>
                                    @endforelse


                                </div>
                            </div>
                            <div class="col-md-9">
                                <div class="">
                                    <button id="show-modal" @click="showModal = true">Show Modal</button>
                                    <!-- use the modal component, pass in the prop -->
                                    <modal v-if="showModal" @close="showModal = false">
                                    <!--
                                      you can use custom content here to overwrite
                                      default content
                                    -->
                                    <h3 slot="header">custom header</h3>
                                    </modal>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('style')
    <link href="{{url('css/jquery.growl.css')}}" rel="stylesheet" type="text/css" />
    <style>
        .konten {
            margin-top: 20px;
        }
        .konten > .row {
            margin-right: 20px;
            margin-left: 30px;
        }
        .row{
            margin-top: 10px;
            margin-bottom: 10px;
        }

        .modal-mask {
            position: fixed;
            z-index: 9998;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background-color: rgba(0, 0, 0, .5);
            display: table;
            transition: opacity .3s ease;
        }

        .modal-wrapper {
            display: table-cell;
            vertical-align: middle;
        }

        .modal-container {
            width: 300px;
            margin: 0px auto;
            padding: 20px 30px;
            background-color: #fff;
            border-radius: 2px;
            box-shadow: 0 2px 8px rgba(0, 0, 0, .33);
            transition: all .3s ease;
            font-family: Helvetica, Arial, sans-serif;
        }

        .modal-header h3 {
            margin-top: 0;
            color: #42b983;
        }

        .modal-body {
            margin: 20px 0;
        }

        .modal-default-button {
            float: right;
        }

        /*
         * The following styles are auto-applied to elements with
         * transition="modal" when their visibility is toggled
         * by Vue.js.
         *
         * You can easily play with the modal transition by editing
         * these styles.
         */

        .modal-enter {
            opacity: 0;
        }

        .modal-leave-active {
            opacity: 0;
        }

        .modal-enter .modal-container,
        .modal-leave-active .modal-container {
            -webkit-transform: scale(1.1);
            transform: scale(1.1);
        }
    </style>
@endsection
@section('script')
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <script src="https://unpkg.com/vue"></script>
    <script src="{{url('js/jquery.growl.js')}}" type="text/javascript"></script>
    <script>
                @if(request()->session()->has('sukses'))
        var msg = "{{request()->session()->get('sukses')}}";
        $.growl.notice({ title:"Sukses!", message: msg });
                @endif

                @if(request()->session()->has('gagal'))
        var msg = "{{request()->session()->get('gagal')}}";
        $.growl.error({ title:"Gagal!", message: msg });
        @endif
    </script>
    <script type="text/x-template" id="modal-template">
        <transition name="modal">
            <div class="modal-mask">
                <div class="modal-wrapper">
                    <div class="modal-container">

                        <div class="modal-header">
                            <slot name="header">
                                default header
                            </slot>
                        </div>

                        <div class="modal-body">
                            <slot name="body">
                                default body
                            </slot>
                        </div>

                        <div class="modal-footer">
                            <slot name="footer">
                                default footer
                                <button class="modal-default-button" @click="$emit('close')">
                                    OK
                                </button>
                            </slot>
                        </div>
                    </div>
                </div>
            </div>
        </transition>
    </script>
    <script>

        Vue.component('modal', {
            template: '#modal-template'
        });

        var app = new Vue({
            el: "#penempatan_board",
            data: {
                showModal: false,
                jenis_kegiatan: [],
                items: [
                    { message: 'Foo' },
                    { message: 'Bar' }
                ]
            },
            created: function(){
                axios.get('/admin/get_data_jenis').then(function(res){
                    this.jenis_kegiatan = res.data;
                });

                console.log(this.items);
            }
        });

    </script>

@endsection