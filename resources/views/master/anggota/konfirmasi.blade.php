@extends('layouts.app')

@section('content')
    <div class="konten">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <h4 class="panel-title">Konfirmasi Anggota</h4>
                    </div>
                    <div class="panel-body">
                        <form action="">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="npm">NPM</label>
                                        <input type="text" id="npm" name="npm" class="form-control" value="{{$anggota->npm}}" readonly>
                                    </div>
                                    <div class="form-group">
                                        <label for="nama_lengkap">Nama Lengkap</label>
                                        <input type="text" id="nama_lengkap" name="nama_lengkap" class="form-control" value="{{$anggota->nama_lengkap}}" readonly>
                                    </div>
                                    <div class="form-group">
                                        <label for="jenis_kelamin">Jenis Kelamin</label>
                                        <input type="text" id="jenis_kelamin" name="jenis_kelamin" class="form-control" value="{{$anggota->jenis_kelamin == 'l' ? 'Laki-Laki' : 'Perempuan'}}" readonly>
                                    </div>
                                    <div class="form-group">
                                        <label for="ttl">Tempat/Tgl Lahir</label>
                                        <input type="text" id="ttl" name="jenis_kelamin" class="form-control" value="{{$anggota->tempat_lahir.', '.date('d-m-Y', strtotime($anggota->tanggal_lahir))}}" readonly>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="gambar text-center">
                                        <img src="{{url('admin/uploads/anggota').'/'.$anggota->id}}" class="img img-thumbnail" width="200" alt="">
                                    </div>
                                    <div class="form-group">
                                        <label for="ttl">Tempat/Tgl Lahir</label>
                                        <input type="text" id="ttl" name="jenis_kelamin" class="form-control" value="{{$anggota->tempat_lahir.', '.date('d-m-Y', strtotime($anggota->tanggal_lahir))}}" readonly>
                                    </div>
                                </div>
                            </div>
                            
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('style')
    <style>
        .konten {
            margin-top: 20px;
        }
        .konten > .row {
            margin-right: 20px;
            margin-left: 30px;
        }
        .row{
            margin-top: 10px;
            margin-bottom: 10px;
        }
    </style>
@endsection

@section('script')

@endsection