@extends('layouts.app')

@section('content')
    <div class="konten">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title"><i class="fa fa-users"></i>&nbsp; &nbsp;List Anggota</h4>
                    </div>

                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="pull-right">
                                    <a href="{{url('/admin/anggota/tambah')}}" class="btn btn-primary btn-sm"><i class="fa fa-user-plus"></i>&nbsp;&nbsp;TAMBAH ANGGOTA</a>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <thead>
                                        <tr>
                                            <th class="text-center" width="10%">NIM</th>
                                            <th class="text-center" width="20%">NAMA LENGKAP</th>
                                            <th class="text-center" width="10%">JENIS KELAMIN</th>
                                            <th class="text-center" width="15%">TEMPAT LAHIR</th>
                                            <th class="text-center" width="15%">TANGGAL LAHIR</th>
                                            <th class="text-center" width="10%">STATUS</th>
                                            <th class="text-center" width="25%">OPSI</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @forelse($data_anggota as $anggota)
                                            <tr>
                                                <td align="center">{{$anggota->npm}}</td>
                                                <td align="center">{{$anggota->nama_lengkap}}</td>
                                                <td align="center">{{$anggota->jenis_kelamin == 'l'? 'Laki-Laki': 'Perempuan'}}</td>
                                                <td align="center">{{$anggota->tempat_lahir}}</td>
                                                <td align="center">{{date('d-m-Y', strtotime($anggota->tanggal_lahir))}}</td>
                                                <td align="center">{!! $anggota->status? '<span class="lbl lbl-primary">Aktif</span>': '<span class="lbl lbl-primary">Aktif</span>' !!}</td>
                                                <td align="center">
                                                    <div class="text-center">
                                                        <a href="{{url('/admin/anggota/update').'/'.$anggota->id}}" class="btn btn-primary btn-sm" title="Update Anggota"><i class="fa fa-pencil"></i></a>
                                                        <a href="{{url('/admin/anggota/view').'/'.$anggota->id}}" title="Lihat Anggota" class="btn btn-info btn-sm"><i class="fa fa-search"></i></a>
                                                        <a href="{{url('/admin/anggota/hapus').'/'.$anggota->id}}" class="btn btn-danger btn-sm"  onclick="if(confirm('Apakah anda yakin dengan ini?')){ return true; } else return false;" title="Hapus Anggota"><i class="fa fa-trash"></i></a>
                                                    </div>
                                                </td>
                                            </tr>
                                        @empty
                                            <tr>
                                                <td colspan="7" align="center">TIDAK ADA DATA ANGGOTA</td>
                                            </tr>
                                        @endforelse
                                        </tbody>
                                    </table>
                                    <div class="text-center">
                                        {{$data_anggota->links()}}

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('style')
    <link href="{{url('css/jquery.growl.css')}}" rel="stylesheet" type="text/css" />
    <style>
        .konten {
            margin-top: 20px;
        }
        .konten > .row {
            margin-right: 20px;
            margin-left: 30px;
        }
        .row{
            margin-top: 10px;
            margin-bottom: 10px;
        }
    </style>
@endsection
@section('script')
    <script src="{{url('js/jquery.growl.js')}}" type="text/javascript"></script>
    <script>
        @if(request()->session()->has('sukses'))
            var msg = "{{request()->session()->get('sukses')}}";
            $.growl.notice({ title:"Sukses!", message: msg });
        @endif

        @if(request()->session()->has('gagal'))
            var msg = "{{request()->session()->get('gagal')}}";
            $.growl.error({ title:"Gagal!", message: msg });
        @endif
    </script>
@endsection