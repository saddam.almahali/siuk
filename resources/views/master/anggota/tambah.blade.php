@extends('layouts.app')
@section('content')
    <div class="konten" id="form-tambah">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h4 class="panel-title"><i class="fa fa-user-plus"></i>&nbsp;&nbsp;Form Tambah Anggota</h4>
                    </div>
                    <div class="panel-body">
                        <form action="{{url('/admin/anggota/tambah')}}" enctype="multipart/form-data" method="post">
                            {{csrf_field()}}
                            <div class="row">

                                <div class="col-md-4 col-md-offset-4 text-center">
                                    <div class="img-wrapper">
                                        <img id="foto_profil" src="{{url('images/uploads/default-user.png')}}" height="200px" width="200px" class="img img-thumbnail" alt="">
                                    </div>
                                    <div class="img-overlay">
                                        <input id="fileInput" type="file" name="photo" value=""  v-on:change="pilih_gambah" style="display:none;" />
                                        <button class="btn btn-md btn-success" onclick=" event.preventDefault(); document.getElementById('fileInput').click()">Browse</button>
                                    </div>
                                </div>

                            </div>
                            <div>

                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs" role="tablist">
                                    <li role="presentation" class="active"><a href="#data_pribadi" aria-controls="data_pribadi" role="tab" data-toggle="tab"><i class="fa fa-info-circle"></i> &nbsp;&nbsp;Data Pokok</a></li>
                                    <li role="presentation"><a href="#alamat" aria-controls="alamat" role="tab" data-toggle="tab"><i class="fa fa-street-view"></i> &nbsp;&nbsp;Alamat</a></li>
                                    <li role="presentation"><a href="#kontak" aria-controls="kontak" role="tab" data-toggle="tab"><i class="fa fa-phone"></i> &nbsp;&nbsp;Kontak</a></li>
                                    <li role="presentation"><a href="#catatan" aria-controls="catatan" role="tab" data-toggle="tab">Catatan</a></li>
                                </ul>

                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane active" id="data_pribadi">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group ">
                                                    <input type="text" name="tanggal_daftar" class="form-control input-lg" value="{{date('d-m-Y')}}" readonly>
                                                </div>
                                                <div class="form-group {{$errors->has('npm') ? 'has-error': ''}}">
                                                    <input type="text" name="npm" class="form-control input-lg" value="{{old('npm')}}" placeholder="Masukan NPM Anda">
                                                    @if($errors->has('npm'))
                                                        <span class="help-block">
                                                            <strong>{{$errors->first('npm')}}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                                <div class="form-group {{$errors->has('nama_lengkap') ? 'has-error' : ''}}">
                                                    <input type="text" name="nama_lengkap" class="form-control input-lg" value="{{old('nama_lengkap')}}" placeholder="Nama Lengkap">
                                                    @if($errors->has('nama_lengkap'))
                                                        <span class="help-block">
                                                            <strong>{{$errors->first('nama_lengkap')}}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group {{$errors->has('jenis_kelamin') ? 'has-error' : ''}}">
                                                    <select name="jenis_kelamin" class="form-control input-lg" id="jenis_kelamin">
                                                        <option value="" disabled>Jenis Kelamin</option>
                                                        <option value="l">Laki-Laki</option>
                                                        <option value="p">Perempuan</option>
                                                    </select>
                                                    @if($errors->has('jenis_kelamin'))
                                                        <span class="help-block">
                                                            <strong>{{$errors->first('jenis_kelamin')}}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                                <div class="form-group {{$errors->has('tempat_lahir') ? 'has-error' : ''}}">
                                                    <input type="text" name="tempat_lahir" class="form-control input-lg" value="{{old('tempat_lahir')}}" placeholder="Tempat Lahir">
                                                    @if($errors->has('tempat_lahir'))
                                                        <span class="help-block">
                                                            <strong>{{$errors->first('tempat_lahir')}}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                                <div class="form-group {{$errors->has('tanggal_lahir') ? 'has-error' : ''}}">
                                                    <input type="text" name="tanggal_lahir" class="form-control input-lg" value="{{old('tanggal_lahir')}}" placeholder="Tanggal Lahir">
                                                    @if($errors->has('tanggal_lahir'))
                                                        <span class="help-block">
                                                            <strong>{{$errors->first('tanggal_lahir')}}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="alamat">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group {{$errors->has('alamat_asal') ? 'has-error' : ''}}">
                                                    <textarea name="alamat_asal" id="alamat_asal" class="form-control input-lg" rows="4" placeholder="Alamat Asal"></textarea>
                                                    @if($errors->has('alamat_asal'))
                                                        <span class="help-block">
                                                            <strong>{{$errors->first('alamat_asal')}}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group {{$errors->has('alamat_kos') ? 'has-error' : ''}}">
                                                    <textarea name="alamat_kos" id="alamat_kos" class="form-control input-lg" rows="4" placeholder="Alamat Kos"></textarea>
                                                    @if($errors->has('alamat_kos'))
                                                        <span class="help-block">
                                                            <strong>{{$errors->first('alamat_kos')}}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="kontak">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <div class="input-group {{$errors->has('email') ? 'has-error':''}}">
                                                        <span class="input-group-addon" id="email-add ">@</span>
                                                        <input type="text" class="form-control input-lg" name="email" placeholder="Email" aria-describedby="email-add">
                                                        @if($errors->has('email'))
                                                            <span class="help-blok">
                                                                <strong>{{$errors->first('email')}}</strong>
                                                            </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <span class="input-group-addon" id="phone-add "><i class="fa fa-phone"></i></span>
                                                        <input type="text" class="form-control input-lg" name="telp" placeholder="No Telp" aria-describedby="phone-add">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <span class="input-group-addon" id="facebook-add "><i class="fa fa-facebook"></i></span>
                                                        <input type="text" class="form-control input-lg" name="facebook" placeholder="Facebook URL" aria-describedby="facebook-add">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <span class="input-group-addon" id="twitter-add"><i class="fa fa-twitter"></i></span>
                                                        <input type="text" class="form-control input-lg" name="twitter" placeholder="Twitter URL" aria-describedby="twitter-add">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <span class="input-group-addon" id="bbm-add">BBM</span>
                                                        <input type="text" class="form-control input-lg" name="twitter" placeholder="PIN BBM" aria-describedby="bbm-add">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="catatan">
                                        <div class="row">
                                            <div class="col-md-8 col-md-offset-2">
                                                <div class="form-group">
                                                    <input type="text" name="hobi" class="form-control input-lg" placeholder="Hobi Anda (Opsional)">
                                                </div>
                                                <div class="form-group">
                                                    <textarea name="moto" id="moto" class="form-control input-lg" rows="3" placeholder="Moto Hidup Anda (Opsional)"></textarea>
                                                </div>
                                                <div class="form-group">
                                                    <textarea name="harapan" id="harapan" class="form-control input-lg" rows="3" placeholder="Harapan Anda Mengikuti UKM Seni (Opsional)"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="pull-right">
                                        <input type="submit" class="btn btn-primary btn-sm" value="Simpan" value="Simpan">
                                    </div>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('style')
    <link rel="stylesheet" href="{{url('/css/bootstrap-datepicker3.min.css')}}">
    <style>
        span.month{
            color: #8d8d8d;
        }
        .konten {
            margin-top: 20px;
        }
        .konten > .row {
            margin-right: 20px;
            margin-left: 30px;
        }
        .row{
            margin-top: 10px;
            margin-bottom: 10px;
        }
        .bootstrap-select > .btn {
            height: 45px;
        }

        .img-wrapper {
            position: relative;
            width: 100%;
        }

        .img-overlay {
            position: absolute;
            top: 0;
            bottom: 0;
            left: 0;
            right: 0;
            text-align: center;
        }

        .img-overlay>button {
            width:200px;
            background-color: transparent;
            border-color:  #2a2a2a;
        }


        .img-overlay:before {
            content: ' ';
            display: block;
            /* adjust 'height' to position overlay content vertically */
            height: 33%;
        }

    </style>
@endsection

@section('script')
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <script src="https://unpkg.com/vue"></script>
    <script src="{{url('js/app.js')}}"></script>
    <script src="{{url('js/bootstrap-datepicker.min.js')}}"></script>
    <script>
        $('input[name="tanggal_lahir"]').datepicker({
            clearBtn: true,
            autoclose: true,
            format: 'dd-mm-yyyy'
        });
    </script>
@endsection