@extends('layouts.app')

@section('content')
    <div class="konten">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <i class="fa fa-user"></i>&nbsp;&nbsp;Detile Anggota
                            <div class="pull-right">
                                <a href="" class="btn btn-warning btn-sm" onclick="event.preventDefault(); location.href='/admin/pendaftaran'"><i class="fa fa-arrow-left"></i> &nbsp;&nbsp;Kembali</a>

                            </div>
                        </h4>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-3 text-center">
                                <div class="img-hover-effect">
                                    <div>
                                        <img data-src="{{url('/admin/pendaftaran/uploads').'/'.$anggota->id}}" class="img img-responsive img-thumbnail b-lazy" id="foto-profil">
                                        <div class="social-links">
                                            <a href="{{$anggota->facebook}}" target="_blank"> <i class="fa fa-facebook"></i> </a>
                                            <a href="#"> <i class="fa fa-twitter"></i> </a>
                                            <a href="#"> <i class="fa fa-linkedin"></i> </a>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="col-md-9">
                                <div>

                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li role="presentation" class="active"><a href="#data_pribadi" aria-controls="data_pribadi" role="tab" data-toggle="tab"><i class="fa fa-info-circle"></i> &nbsp;&nbsp;Data Pokok</a></li>
                                        <li role="presentation"><a href="#catatan" aria-controls="catatan" role="tab" data-toggle="tab"><i class="fa fa-book"></i> &nbsp;&nbsp;Catatan</a></li>
                                        <li role="presentation"><a href="#anggota" aria-controls="anggota" role="tab" data-toggle="tab"><i class="fa fa-users"></i> &nbsp;&nbsp;Keanggotaan</a></li>
                                    </ul>

                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active" id="data_pribadi">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <table class="table table-borderless">
                                                        <tr>
                                                            <th width="35%">NPM</th>
                                                            <td align="center" style="width: 2%;">:</td>
                                                            <td>{{$anggota->npm}}</td>
                                                        </tr>
                                                        <tr>
                                                            <th  width="20%">Nama Lengkap</th>
                                                            <td align="center" style="width: 2%;">:</td>
                                                            <td>{{$anggota->nama_lengkap}}</td>
                                                        </tr>
                                                        <tr>
                                                            <th  width="20%">Jenis Kelamin</th>
                                                            <td align="center" style="width: 2%;">:</td>
                                                            <td>{{$anggota->jenis_kelamin == 'l'? 'Laki-Laki': 'Perempuan'}}</td>
                                                        </tr>
                                                        <tr>
                                                            <th  width="20%">Tempat Lahir</th>
                                                            <td align="center" style="width: 2%;">:</td>
                                                            <td>{{$anggota->tempat_lahir}}</td>
                                                        </tr>
                                                        <tr>
                                                            <th  width="20%">Tanggal Lahir</th>
                                                            <td align="center" style="width: 2%;">:</td>
                                                            <td>{{date('d-m-Y', strtotime($anggota->tanggal_lahir))}}</td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div class="col-md-6">
                                                    <table class="table table-borderless">
                                                        <tr>
                                                            <th width="35%">Alamat Asal</th>
                                                            <td align="center" style="width: 2%;">:</td>
                                                            <td>{{$anggota->alamat_asal}}</td>
                                                        </tr>
                                                        <tr>
                                                            <th  width="20%">Alamat Kos</th>
                                                            <td align="center" style="width: 2%;">:</td>
                                                            <td>{{$anggota->alamat_kos}}</td>
                                                        </tr>
                                                        <tr>
                                                            <th  width="20%">Hobi</th>
                                                            <td align="center" style="width: 2%;">:</td>
                                                            <td>{{$anggota->hobi}}</td>
                                                        </tr>
                                                    </table>
                                                </div>

                                            </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="catatan">
                                            <div class="row">
                                                <div class="col-md-8 col-md-offset-2">
                                                    <div class="form-group">
                                                        <textarea name="moto" id="moto" class="form-control input-lg" rows="3" placeholder="Moto Hidup Anda (Opsional)" readonly>{{$anggota->moto}}</textarea>
                                                    </div>
                                                    <div class="form-group">
                                                        <textarea name="harapan" id="harapan" class="form-control input-lg" rows="3" placeholder="Harapan Anda Mengikuti UKM Seni (Opsional)" readonly>{{$anggota->harapan}}</textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="anggota">
                                            <div class="row">
                                                <div class="col-md-8 col-md-offset-2">
                                                    <div class="page-header">
                                                        <h3>Keanggotaan yang diikuti</h3>
                                                    </div>
                                                    <div>
                                                        <ol>
                                                            @foreach($anggota->jenis as $jenis)
                                                                <li>{{strtoupper($jenis->nama_jenis)}}</li>
                                                            @endforeach
                                                        </ol>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('style')
    <style>

        .b-lazy {
            opacity:0;
            transform: scale(3);
            transition: all 500ms;
        }
        .b-loaded {
            opacity:1;
            transform: scale(1);
        }
        .konten {
            margin-top: 20px;
        }
        .konten > .row {
            margin-right: 20px;
            margin-left: 30px;
        }
        .row{
            margin-top: 10px;
            margin-bottom: 10px;
        }
        .table-borderless > tbody > tr > td,
        .table-borderless > tbody > tr > th,
        .table-borderless > tfoot > tr > td,
        .table-borderless > tfoot > tr > th,
        .table-borderless > thead > tr > td,
        .table-borderless > thead > tr > th {
            border: none;
        }
    </style>
@endsection
@section('script')
    <script src="{{url('/js/blazy.min.js')}}" type="text/javascript"></script>
    <script>
        var bLazy = new Blazy({
            breakpoints: [{
                width: 420 // Max-width
            }]
            , success: function(element){
                setTimeout(function(){
                    // We want to remove the loader gif now.
                    // First we find the parent container
                    // then we remove the "loading" class which holds the loader image

                }, 200);
            }
        });
    </script>
@endsection