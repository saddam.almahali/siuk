@extends('layouts.app')

@section('content')
    <div class="konten">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title"><i class="fa fa-users"></i>&nbsp; &nbsp;List Anggota Belum Terkonfirmasi</h4>
                    </div>

                    <div class="panel-body">

                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <thead>
                                        <tr>
                                            <th class="text-center" width="10%">NIM</th>
                                            <th class="text-center" width="20%">NAMA LENGKAP</th>
                                            <th class="text-center" width="10%">JENIS KELAMIN</th>
                                            <th class="text-center" width="15%">TEMPAT LAHIR</th>
                                            <th class="text-center" width="15%">TANGGAL LAHIR</th>
                                            <th class="text-center" width="10%">STATUS</th>
                                            <th class="text-center" width="25%">OPSI</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @forelse($data_anggota as $anggota)
                                            <tr>
                                                <td align="center" style="vertical-align: middle;">{{$anggota->npm}}</td>
                                                <td align="center" style="vertical-align: middle;">{{$anggota->nama_lengkap}}</td>
                                                <td align="center" style="vertical-align: middle;">{{$anggota->jenis_kelamin == 'l'? 'Laki-Laki': 'Perempuan'}}</td>
                                                <td align="center" style="vertical-align: middle;">{{$anggota->tempat_lahir}}</td>
                                                <td align="center" style="vertical-align: middle;">{{date('d-m-Y', strtotime($anggota->tanggal_lahir))}}</td>
                                                <td align="center" style="vertical-align: middle;">{!! $anggota->status? '<span class="lbl lbl-primary">Aktif</span>': '<span class="lbl lbl-primary">Aktif</span>' !!}</td>
                                                <td align="center">
                                                    <div class="text-center">
                                                        <a href="{{url('/admin/pendaftaran/detile').'/'.$anggota->id}}" class="btn btn-primary btn-sm" title="Detile Calon Anggota"><i class="fa fa-search"></i></a>
                                                        <a href="{{url('/admin/pendaftaran/konfirmasi').'/'.$anggota->id}}" class="btn btn-success btn-sm" title="Konfirmasi Calon Anggota"><i class="fa fa-calendar-o"></i></a>

                                                    </div>
                                                </td>
                                            </tr>
                                        @empty
                                            <tr>
                                                <td colspan="7" align="center">TIDAK ADA DATA ANGGOTA</td>
                                            </tr>
                                        @endforelse
                                        </tbody>
                                    </table>
                                    <div class="text-center">
                                        {{$data_anggota->links()}}

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('style')
    <link href="{{url('css/jquery.growl.css')}}" rel="stylesheet" type="text/css" />
    <style>
        .konten {
            margin-top: 20px;
        }
        .konten > .row {
            margin-right: 20px;
            margin-left: 30px;
        }
        .row{
            margin-top: 10px;
            margin-bottom: 10px;
        }
        tr td{
            vertical-align: middle;
        }
    </style>
@endsection
@section('script')
    <script src="{{url('js/jquery.growl.js')}}" type="text/javascript"></script>
    <script>
                @if(request()->session()->has('sukses'))
        var msg = "{{request()->session()->get('sukses')}}";
        $.growl.notice({ title:"Sukses!", message: msg });
                @endif

                @if(request()->session()->has('gagal'))
        var msg = "{{request()->session()->get('gagal')}}";
        $.growl.error({ title:"Gagal!", message: msg });
        @endif
    </script>
@endsection