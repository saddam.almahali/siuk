@extends('layouts.app')

@section('content')
    <div class="konten">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h4 class="panel-title">List Jenis Kegiatan</h4>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <a href="{{url('/admin/jenis_kegiatan/tambah')}}" class="btn btn-primary btn-sm pull-right">Tambah Jenis</a>
                            </div>
                        </div>
                        <table class="table table-bordered table">
                            <thead>
                            <tr>
                                <th style="text-align: center" width="10%">#</th>
                                <th style="text-align: center" width="15%">Kode Jenis</th>
                                <th style="text-align: center" width="20%">Nama Jenis</th>
                                <th style="text-align: center" >Keterangan</th>
                                <th style="text-align: center" width="10%">Jumlah Anggota</th>
                                <th style="text-align: center" width="20%">Opsi</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $i = 0; ?>
                            @forelse($data_jenis as $jenis)
                                <tr>
                                    <td align="center">{{$i + 1}}</td>
                                    <td align="center">{{$jenis->kode_jenis}}</td>
                                    <td align="center">{{$jenis->nama_jenis}}</td>
                                    <td align="center">{{$jenis->keterangan}}</td>
                                    <td align="center">{{$jenis->anggota()->count()}}</td>
                                    <td align="center">
                                        <a href="{{url('/admin/jenis_kegiatan/view').'/'.$jenis->id}}" class="btn btn-info btn-sm" title="Detail Jenis Kegiatan"><i class="fa fa-search"></i></a>
                                        <a href="{{url('/admin/jenis_kegiatan/update').'/'.$jenis->id}}" class="btn btn-primary btn-sm" title="Update Anggota"><i class="fa fa-pencil"></i></a>
                                        <a href="{{url('/admin/jenis_kegiatan/hapus').'/'.$jenis->id}}" class="btn btn-danger btn-sm"  onclick="if(confirm('Apakah anda yakin dengan ini?')){ return true; } else return false;" title="Hapus Anggota"><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="5" align="center">Tidak Ada Data Jenis Kegiatan</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>


                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('style')
    <link href="{{url('css/jquery.growl.css')}}" rel="stylesheet" type="text/css" />
    <style>
        .konten {
            margin-top: 20px;
        }
        .konten > .row {
            margin-right: 20px;
            margin-left: 30px;
        }
        .row{
            margin-top: 10px;
            margin-bottom: 10px;
        }
    </style>
@endsection

@section('script')
    <script src="{{url('js/jquery.growl.js')}}" type="text/javascript"></script>
    <script>
        @if(request()->session()->has('sukses'))
            var msg = "{{request()->session()->get('sukses')}}";
            $.growl.notice({ title:"Sukses!", message: msg });
        @endif
        @if(request()->session()->has('gagal'))
            var msg = "{{request()->session()->get('gagal')}}";
            $.growl.error({ title:"Gagal!", message: msg });
        @endif
    </script>
@endsection