@extends('layouts.app')

@section('content')
    <div class="konten">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h4 class="panel-title">Tambah Jenis Kegiatan</h4>
                    </div>
                    <div class="panel-body">
                        <form action="{{url('/admin/jenis_kegiatan/simpan')}}" method="post">
                            {{csrf_field()}}
                            <div class="form-group {{$errors->has('kode_jenis') ? 'has-error' : ''}}">
                                <input type="text" class="form-control input-sm" name="kode_jenis" value="{{old('kode_jenis')}}" placeholder="Kode Jenis">
                                @if($errors->has('kode_jenis'))
                                    <span class="help-block">
                                        <strong>{{$errors->first('kode_jenis')}}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group {{$errors->has('nama_jenis') ? 'has-error' : ''}}">
                                <input type="text" class="form-control input-sm" value="{{old('nama_jenis')}}" name="nama_jenis" placeholder="Nama Jenis">
                                @if($errors->has('nama_jenis'))
                                    <span class="help-block">
                                        <strong>{{$errors->first('nama_jenis')}}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group {{$errors->has('keterangan') ? 'has-error' : ''}}">
                                <textarea name="keterangan" id="keterangan" class="form-control input-sm" rows="3" placeholder="Keterangan Kegiatan"> {{old('keterangan')}}</textarea>
                                @if($errors->has('keterangan'))
                                    <span class="help-block">
                                        <strong>{{$errors->first('keterangan')}}</strong>
                                    </span>
                                @endif
                            </div>

                            <input type="submit" class="btn btn-primary btn-sm pull-right" style="margin-left:15px; width:100px;" value="Simpan">
                            <input type="submit" class="btn btn-danger btn-sm pull-right" style="width: 100px" onclick="event.preventDefault(); history.back();" value="Batal">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('style')
    <style>
        .konten {
            margin-top: 20px;
        }
        .konten > .row {
            margin-right: 20px;
            margin-left: 30px;
        }
        .row{
            margin-top: 10px;
            margin-bottom: 10px;
        }
    </style>
@endsection

@section('script')

@endsection