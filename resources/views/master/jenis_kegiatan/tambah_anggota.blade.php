@extends('layouts.app')

@section('content')
    <div class="konten">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h4 class="panel-title">Tambah Angota</h4>
                    </div>
                    <div class="panel-body">
                        <form action="{{url('/admin/jenis_kegiatan/simpan_anggota')}}" method="post">
                            {{csrf_field()}}
                            <input type="hidden" name="id_jenis" value="{{$jenis_kegiatan->id}}">
                            <div class="form-group {{$errors->has('nama_jenis') ? 'has-error' : ''}}">
                                <input type="text" class="form-control input-sm" name="kode_jenis" value="{{$jenis_kegiatan->nama_jenis}}" readonly>
                                @if($errors->has('nama_jenis'))
                                    <span class="help-block">
                                        <strong>{{$errors->first('nama_jenis')}}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group {{$errors->has('anggota') ? 'has-error' : ''}}">
                                <select name="anggota" id="anggota" class="form-control">
                                    <option value="" disabled selected>Pilih Anggota</option>
                                    @foreach($data_anggota as $anggota)
                                        <option value="{{$anggota->id}}">{{$anggota->npm.' | '.$anggota->nama_lengkap}}</option>
                                    @endforeach
                                </select>
                                @if($errors->has('anggota'))
                                    <span class="help-block">
                                        <strong>{{$errors->first('anggota')}}</strong>
                                    </span>
                                @endif
                            </div>


                            <input type="submit" class="btn btn-primary btn-sm pull-right" style="margin-left:15px; width:100px;" value="Simpan">
                            <input type="submit" class="btn btn-danger btn-sm pull-right" style="width: 100px" onclick="event.preventDefault(); history.back();" value="Batal">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('style')

    <link rel="stylesheet" href="{{url('/css/select2.min.css')}}">
    <style>
        .konten {
            margin-top: 20px;
        }
        .konten > .row {
            margin-right: 20px;
            margin-left: 30px;
        }
        .row{
            margin-top: 10px;
            margin-bottom: 10px;
        }
    </style>
@endsection

@section('script')
    <script src="{{url('/js/select2.min.js')}}"></script>

    <script>
        $('#anggota').select2();
    </script>

@endsection