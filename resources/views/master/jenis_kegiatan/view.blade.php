@extends('layouts.app')

@section('content')
    <div class="konten">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <i class="fa fa-user"></i>&nbsp;&nbsp;Detile Anggota
                            <div class="pull-right">
                                <a href="" class="btn btn-warning btn-sm" onclick="event.preventDefault(); history.back();"><i class="fa fa-arrow-left"></i> &nbsp;&nbsp;Kembali</a>

                            </div>
                        </h4>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div>

                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li role="presentation"><a href="#detail" aria-controls="detail" role="tab" data-toggle="tab">Detail Jenis Kegiatan</a></li>
                                        <li role="presentation"><a href="#anggota" aria-controls="anggota" role="tab" data-toggle="tab">Keanggotaan</a></li>
                                    </ul>

                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active" id="detail">
                                            <div class="form-group">
                                                <label for="">Kode Jenis</label>
                                                <input type="text" readonly class="form-control" value="{{$jenis_kegiatan->kode_jenis}}">
                                            </div>
                                            <div class="form-group">
                                                <label for="nama_jenis">Nama Jenis</label>
                                                <input type="text" readonly class="form-control" id="nama_jenis" value="{{$jenis_kegiatan->nama_jenis}}">
                                            </div>
                                            <div class="form-group">
                                                <label for="keterangan">Keterangan</label>
                                                <textarea name="keterangan" id="keterangan" class="form-control" rows="5" readonly>{{$jenis_kegiatan->keterangan}}</textarea>
                                            </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="anggota">
                                            <div class="page-header">
                                                <h4>
                                                    List Keanggotaan
                                                    <a href="{{url('/admin/jenis_kegiatan/tambah_anggota').'/'.$jenis_kegiatan->id}}" class="btn btn-sm btn-info pull-right">Tambah Anggota</a>
                                                </h4>
                                            </div>
                                            <div class="table-responsive">
                                                <table class="table table-bordered">
                                                    <thead>
                                                    <tr>
                                                        <th style="width: 4%; text-align:center;">#</th>
                                                        <th style="width: 10%; text-align:center;">NPM</th>
                                                        <th style="width: 20%; text-align:center;">Nama</th>
                                                        <th style="width: 10%; text-align:center;">Jenis Kelamin</th>
                                                        <th style="width: 25%; text-align:center;">Tpt/Tanggal Lahir</th>
                                                        <th style="width: 10%; text-align:center;">Status</th>
                                                        <th style="text-align:center;">Opsi</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php $i=0; ?>
                                                    @forelse($data_anggota as $anggota)
                                                        <tr>
                                                            <td align="center">{{$i+1}}</td>
                                                            <td align="center">{{$anggota->npm}}</td>
                                                            <td align="center">{{$anggota->nama_lengkap}}</td>
                                                            <td align="center">{{$anggota->jenis_kelamin=='l'? 'Laki-Laki':'Perempuan'}}</td>
                                                            <td align="center">{{$anggota->tempat_lahir.', '.date('d-m-Y', strtotime($anggota->tanggal_lahir))}}</td>
                                                            <td align="center">{{$anggota->status ? 'Aktif':'Tidak Aktif'}}</td>
                                                            <td align="center"><a
                                                                        href="{{url('/admin/jenis_kegiatan/view/unlink_anggota').'/'.$jenis_kegiatan->id.'/'.$anggota->id}}"
                                                                        onclick="if(confirm('Apakah Anda Yakin Ingin Mengeluarkan Anggota ?')){return true;} else return false;"
                                                                        class="btn btn-danger btn-sm" title="Keluarkan Anggota"><i class="fa fa-sign-out"></i></a></td>
                                                        </tr>
                                                        <?php $i++; ?>
                                                    @empty
                                                        <tr>
                                                            <td colspan="7" align="center">Tidak Terdapat Anggota Pada Kegiatan Ini</td>
                                                        </tr>
                                                    @endforelse
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('style')
    <link href="{{url('css/jquery.growl.css')}}" rel="stylesheet" type="text/css" />
    <style>
        .konten {
            margin-top: 20px;
        }
        .konten > .row {
            margin-right: 20px;
            margin-left: 30px;
        }
        .row{
            margin-top: 10px;
            margin-bottom: 10px;
        }
        .table-borderless > tbody > tr > td,
        .table-borderless > tbody > tr > th,
        .table-borderless > tfoot > tr > td,
        .table-borderless > tfoot > tr > th,
        .table-borderless > thead > tr > td,
        .table-borderless > thead > tr > th {
            border: none;
        }
    </style>
@endsection
@section('script')
    <script src="{{url('js/jquery.growl.js')}}" type="text/javascript"></script>
    <script>
                @if(request()->session()->has('sukses'))
        var msg = "{{request()->session()->get('sukses')}}";
        $.growl.notice({ title:"Sukses!", message: msg });
                @endif
                @if(request()->session()->has('gagal'))
        var msg = "{{request()->session()->get('gagal')}}";
        $.growl.error({ title:"Gagal!", message: msg });
        @endif
    </script>
@endsection