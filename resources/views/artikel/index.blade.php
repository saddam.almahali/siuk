@extends('layouts.app')

@section('content')
    <section class="container">
        <div class="row">
            <div class="col-md-9 blog-grid">
                <section class="blog-services">
                    <div class="row news">
                        <?php $jml = 1; ?>
                        @forelse($data_posting as $posting)
                            <div class="col-sm-6">
                                <div class="blog-img-box">
                                    <div class="blog-date"> <span class="month">{{date('M', strtotime($posting->tanggal_posting))}} </span> <span class="date"> {{date('d', strtotime($posting->tanggal_posting))}}</span> </div>
                                    <a class="hover-effect" href="blog-single.html">
                                        <img src="{{url('/uploads/get').'/'.$posting->gambar()->first()->gambar}}" alt="Fuel" />
                                    </a>
                                </div>
                                <div class="blog-content">
                                    <h3><a href="blog-single.html"> {{$posting->judul_posting}} </a> </h3>
                                    <p>By <a href="#">{{$posting->author}}</a> </p>
                                </div>
                            </div>
                            @if($jml == 2){
                                <div class="clearfix spacer-60"> </div>
                                <?php $jml = 1;?>
                            @else
                                <?php $jml++; ?>
                            @endif
                        @empty
                            <div class="text-center">Tidak Terdapat Berita</div>
                        @endforelse
                    </div>
                    <div class="text-center">
                        {{$data_posting->links()}}
                    </div>
                </section>
            </div>

            <div class="col-md-3 sidebar">

                <div class="sidebar-search-form">
                    <input type="text" class="  search-query form-control" placeholder="Search" />
                    <button class="btn search-btn" type="button">
                        <span class="fa fa-search"></span>
                    </button>
                </div>

                <div class="plubication-downloads">
                    <h3 class="publish">&nbsp;&nbsp;<i class="fa fa-calendar"></i>&nbsp;&nbsp;Agenda</h3>

                    <ul class="download-list">
                        @forelse($data_agenda as $agenda)
                            <li><a href="{{url('/manage/agenda/detile').'/'.$agenda->id}}"> {{$agenda->nama_kegiatan}} </a> <span>{{date('d-m-Y', strtotime($agenda->tanggal))}}</span></li>
                        @empty
                            <li class="text-center">Tidak Terdapat Anggota</li>
                        @endforelse
                    </ul>
                </div>
            </div>

        </div>
    </section>
@endsection

@section('style')
    <link href="{{url('css/jquery.growl.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{url('css/bootstrap-datepicker3.min.css')}}" rel="stylesheet" type="text/css" />
    <style>
        .b-lazy {
            opacity:0;
            transform: scale(3);
            transition: all 500ms;
        }
        .b-loaded {
            opacity:1;
            transform: scale(1);
        }
        span.month{
            color: #8d8d8d;
        }
        .konten {
            margin-top: 20px;
        }
        .konten > .row {
            margin-right: 20px;
            margin-left: 30px;
        }
        .row{
            margin-top: 10px;
            margin-bottom: 10px;
        }
        .form-group{
            margin-right: 20px;
        }
    </style>
@endsection
@section('script')
    <script src="{{url('/js/blazy.min.js')}}" type="text/javascript"></script>
    <script src="{{url('js/jquery.growl.js')}}" type="text/javascript"></script>
    <script src="{{url('js/bootstrap-datepicker.min.js')}}" type="text/javascript"></script>

    <script>
                @if(request()->session()->has('sukses'))
        var msg = "{{request()->session()->get('sukses')}}";
        $.growl.notice({ title:"Sukses!", message: msg });
                @endif

                @if(request()->session()->has('gagal'))
        var msg = "{{request()->session()->get('gagal')}}";
        $.growl.error({ title:"Gagal!", message: msg });
                @endif
        var option = {
                clearBtn: true,
                autoclose: true,
                format: 'dd-mm-yyyy'
            };
        $('#dari_tanggal').datepicker(option);
        $('#sampai_tanggal').datepicker(option);
    </script>
    <script>
        var bLazy = new Blazy({
            breakpoints: [{
                width: 420 // Max-width
            }]
            , success: function(element){
                setTimeout(function(){
                    // We want to remove the loader gif now.
                    // First we find the parent container
                    // then we remove the "loading" class which holds the loader image

                }, 200);
            }
        });
    </script>
@endsection