@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-9 blog-info">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="page-header">
                            <h3>TENTANG UKM SENI SEKOLAH TINGGI TEKNOLOGI GARUT</h3>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <img src="{{url('/images/profil.jpeg')}}" class="img img-responsive img-thumbnail" alt="img">
                            </div>
                            <div class="col-md-8">
                                <br />
                                <br />
                                <br />
                                <table class="table table-borderless">
                                    <tr>
                                        <th>NPM</th>
                                        <td style="width: 5%;">:</td>
                                        <td>1306108</td>
                                    </tr>
                                    <tr>
                                        <th>Nama Lengkap</th>
                                        <td style="width: 5%;">:</td>
                                        <td>Ramdani Setiawan</td>
                                    </tr>
                                    <tr>
                                        <th>Tempat/Tgl Lahir</th>
                                        <td style="width: 5%;">:</td>
                                        <td>Garut, 10 November 1991</td>
                                    </tr>
                                    <tr>
                                        <th>Jurusan</th>
                                        <td style="width: 5%;">:</td>
                                        <td>Teknik Informatika</td>
                                    </tr>
                                    <tr>
                                        <th>Dosen Pembimbing</th>
                                        <td style="width: 5%;">:</td>
                                        <td>Asri Mulyani, M.Kom</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-3 sidebar">

                <div class="login-box">
                    <button class="btn btn-info btn-block" onclick="location.href='/login'"><i class="fa fa-key"></i>&nbsp;&nbsp;LOGIN</button>
                    <button class="btn btn-success btn-block" onclick="location.href='/daftar_anggota'"><i class="fa fa-calendar-o"></i>&nbsp;&nbsp;DAFTAR</button>

                </div>

            </div>

        </div>
    </div>
@endsection

@section('style')
    <link href="{{url('css/jquery.growl.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{url('css/bootstrap-datepicker3.min.css')}}" rel="stylesheet" type="text/css" />
    <style>
        .b-lazy {
            opacity:0;
            transform: scale(3);
            transition: all 500ms;
        }
        .b-loaded {
            opacity:1;
            transform: scale(1);
        }
        span.month{
            color: #8d8d8d;
        }
        .konten {
            margin-top: 20px;
        }
        .konten > .row {
            margin-right: 20px;
            margin-left: 30px;
        }
        .row{
            margin-top: 10px;
            margin-bottom: 10px;
        }
        .form-group{
            margin-right: 20px;
        }
        .table-borderless > tbody > tr > td,
        .table-borderless > tbody > tr > th,
        .table-borderless > tfoot > tr > td,
        .table-borderless > tfoot > tr > th,
        .table-borderless > thead > tr > td,
        .table-borderless > thead > tr > th {
            border: none;
        }
    </style>
@endsection
@section('script')
    <script src="{{url('/js/blazy.min.js')}}" type="text/javascript"></script>
    <script src="{{url('js/jquery.growl.js')}}" type="text/javascript"></script>
    <script src="{{url('js/bootstrap-datepicker.min.js')}}" type="text/javascript"></script>

    <script>
                @if(request()->session()->has('sukses'))
        var msg = "{{request()->session()->get('sukses')}}";
        $.growl.notice({ title:"Sukses!", message: msg });
                @endif

                @if(request()->session()->has('gagal'))
        var msg = "{{request()->session()->get('gagal')}}";
        $.growl.error({ title:"Gagal!", message: msg });
                @endif
        var option = {
                clearBtn: true,
                autoclose: true,
                format: 'dd-mm-yyyy'
            };
        $('#dari_tanggal').datepicker(option);
        $('#sampai_tanggal').datepicker(option);
    </script>
    <script>
        var bLazy = new Blazy({
            breakpoints: [{
                width: 420 // Max-width
            }]
            , success: function(element){
                setTimeout(function(){
                    // We want to remove the loader gif now.
                    // First we find the parent container
                    // then we remove the "loading" class which holds the loader image

                }, 200);
            }
        });
    </script>
@endsection