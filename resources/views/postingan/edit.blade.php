@extends('layouts.app')

@section('content')
    <div class="konten">

        <div class="row">
            <form action="{{url('/posting/edit/simpan')}}" method="post" enctype="multipart/form-data">
                {{csrf_field()}}
                <input type="hidden" name="id_posting" value="{{ $posting->id }}">
                <div class="col-md-8">
                    <div class="form-group {{$errors->has('judul_posting') ? 'has-error': ''}}">
                        <input type="text" class="form-control" name="judul_posting" placeholder="Judul Posting" value="{{$posting->judul_posting}}">
                        @if($errors->has('judul_posting'))
                            <span class="help-block">
                                <strong>{{$errors->first('judul_posting')}}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group {{$errors->has('isi_posting') ? 'has-error': ''}}">
                        <textarea name="isi_posting" placeholder="Isi Postingan ini dengan artikel bermanfaat">{{$posting->isi_posting}}</textarea>
                        @if($errors->has('isi_posting'))
                            <span class="help-block">
                                <strong>{{$errors->first('isi_posting')}}</strong>
                            </span>
                        @endif
                    </div>

                </div>
                <div class="col-md-4">
                    <div class="panel">
                        <div class="panel-body">
                            <div class="form-group {{$errors->has('author') ? 'has-error': ''}}">
                                <label for="kategori">Author</label>
                                <input type="text" class="form-control" name="author" readonly value="{{auth()->user()->name}}" placeholder="Nama Author">
                                @if($errors->has('author'))
                                    <span class="help-block">
                                        <strong>{{$errors->first('author')}}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group {{$errors->has('gambar') ? 'has-error': ''}}">
                                <label for="gambar">Gambar Artikel</label>
                                <input type="file" class="form-control" name="gambar">
                                @if($errors->has('gambar'))
                                    <span class="help-block">
                                        <strong>{{$errors->first('gambar')}}</strong>
                                    </span>
                                @endif
                            </div>
                            @if(auth()->user()->hasRole('admin'))
                                <div class="form-group {{$errors->has('status') ? 'has-error': ''}}">
                                    <label for="status">Status</label>
                                    <select name="status" id="status" class="form-control input-sm">
                                        <option value="">Pilih Status</option>
                                        <option value="waiting" {{$posting->approve->status == 'waiting'?'selected':''}}>Waiting</option>
                                        <option value="approval" {{$posting->approve->status == 'approval'?'selected':''}}>Approval</option>
                                        <option value="reject" {{$posting->approve->status == 'reject'?'selected':''}}>Rejected</option>

                                    </select>
                                    @if($errors->has('status'))
                                        <span class="help-block">
                                        <strong>{{$errors->first('status')}}</strong>
                                    </span>
                                    @endif
                                </div>
                            @else
                                <input type="hidden" name="status" value="waiting">
                            @endif

                            <div class="form-group">
                                <input type="submit" class="btn btn-primary btn-sm" value="Simpan">
                            </div>

                        </div>
                    </div>
                </div>
            </form>
        </div>

    </div>
@endsection

@section('style')
    <link href="{{url('css/jquery.growl.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{url('css/bootstrap-datepicker3.min.css')}}" rel="stylesheet" type="text/css" />
    <style>
        span.month{
            color: #8d8d8d;
        }
        .konten {
            margin-top: 20px;
        }
        .konten > .row {
            margin-right: 20px;
            margin-left: 30px;
        }
        .row{
            margin-top: 10px;
            margin-bottom: 10px;
        }
        .form-group{
            margin-right: 20px;
        }
    </style>
@endsection
@section('script')
    <script src="{{url('js/jquery.growl.js')}}" type="text/javascript"></script>
    <script src="{{url('js/bootstrap-datepicker.min.js')}}" type="text/javascript"></script>
    <script src="https://cdn.ckeditor.com/4.7.1/standard/ckeditor.js"></script>
    <script>
                @if(request()->session()->has('sukses'))
        var msg = "{{request()->session()->get('sukses')}}";
        $.growl.notice({ title:"Sukses!", message: msg });
                @endif

                @if(request()->session()->has('gagal'))
        var msg = "{{request()->session()->get('gagal')}}";
        $.growl.error({ title:"Gagal!", message: msg });
                @endif
        var option = {
                clearBtn: true,
                autoclose: true,
                format: 'dd-mm-yyyy'
            };

        CKEDITOR.replace( 'isi_posting' );
    </script>
@endsection