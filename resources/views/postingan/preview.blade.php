@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-9 blog-info">
                <section class="blog-single">
                    <div class="blog-slide">
                        <ul class="slides">
                            <li>
                                <div class="img-responsive">
                                    <img data-src="{{url('/uploads/get').'/'.$posting->gambar()->first()->gambar}}" alt="blog-image 1" class="img img-thumbnail b-lazy" width="666" height="300" />
                                </div>

                            </li>
                        </ul>
                    </div>

                    <div class="single-post">
                        <h4 class="services-title-one subtitle">{{strtoupper($posting->judul_posting)}}</h4>
                        {!! $posting->isi_posting !!}

                    </div>

                    <div class="social-share text-center">
                        <a class="fb-share" href="#"> <i class="fa fa-facebook" aria-hidden="true"></i>Share on Facebook </a>
                        <a class="tweet-share" href="#"> <i class="fa fa-twitter" aria-hidden="true"></i>Share on Twitter </a>
                    </div>

                    @if(!auth()->user()->hasRole('admin'))
                    <div class="author-box">
                        <img data-src="{{url('/uploads/get').'/'.$anggota->foto}}" width="100" class="img b-lazy" alt="Taylor">
                        <div class="author-details">
                            <h4 class="subtitle">{{$anggota->nama_lengkap}}</h4>
                            <p>{{$anggota->moto ? $anggota->moto :'Tidak Terdapat Moto'}} </p>
                            <div class="author-share">
                                @if($anggota->facebook)
                                    <a href="{{$anggota->facebook}}" target="_blank"> <i class="fa fa-facebook" aria-hidden="true"></i> </a>
                                @endif
                                @if($anggota->facebook)
                                        <a href="{{$anggota->twitter}}" target="_blank"> <i class="fa fa-twitter" aria-hidden="true"></i> </a>
                                @endif
                            </div>
                        </div>
                    </div>
                    @endif

                    <div class="comment-box">
                        <h2 class="title-2 text-center"> Add Comment </h2>

                        <form action="comment-form.php" method="POST" id="commentform" class="commentform">
                            <div class='row'>
                                <div id="comment-name" class="col-md-4">
                                    <input type="text" id="comm-name" class="form-control" placeholder="Name" name="comm-name">
                                </div>
                                <div id="comment-email" class="col-md-4">
                                    <input type="email" id="comm-email" class="form-control" placeholder="Email Address" name="comm-email">
                                </div>
                                <div id="comment-url" class="col-md-4">
                                    <input type="url" id="comm-url" class="form-control" placeholder="Website (optional)" name="comm-url">
                                </div>
                                <div id="comment-message" class="col-md-12">
                                    <textarea id="comment" class="form-control" name="comment" placeholder="Message"></textarea>
                                </div>
                                <div class="comment-btn col-md-12">
                                    <button type="submit" class="btn btn-block btn-warning"> ADD COMMENT </button>
                                </div>
                            </div>
                        </form>

                    </div>

                </section>
            </div>

            <div class="col-md-3 sidebar">

                @if(!auth()->user()->hasRole('admin'))
                    <div class="sidebar-fact">
                        <div class="img-responsive">
                            <img data-src="{{url('/uploads/get').'/'.$anggota->foto}}" alt="img" class="img img-thumbnail b-lazy">
                        </div>
                        <h3 class="about-quick-fact text-center">{{$anggota->nama_lengkap}}</h3>
                        <p>{{$anggota->moto ? $anggota->moto : 'Tidak Terdapat Moto'}}</p>
                        

                    </div>
                @endif

                <div class="sidebar-blog-categories">
                    <h3 class="sidebar-title">Categories</h3>

                    <ul>
                        <li> <a href="#">The Company</a> </li>
                        <li> <a href="#">Careers</a> </li>
                        <li> <a href="#">Transportation</a> </li>
                        <li> <a href="#">Environment</a> </li>
                        <li> <a href="#">Refineries</a> </li>
                        <li> <a href="#">Technology</a> </li>
                        <li> <a href="#">Marketing &amp; Sales</a> </li>
                    </ul>

                </div>

            </div>

        </div>
    </div>
@endsection

@section('style')
    <link href="{{url('css/jquery.growl.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{url('css/bootstrap-datepicker3.min.css')}}" rel="stylesheet" type="text/css" />
    <style>
        .b-lazy {
            opacity:0;
            transform: scale(3);
            transition: all 500ms;
        }
        .b-loaded {
            opacity:1;
            transform: scale(1);
        }
        span.month{
            color: #8d8d8d;
        }
        .konten {
            margin-top: 20px;
        }
        .konten > .row {
            margin-right: 20px;
            margin-left: 30px;
        }
        .row{
            margin-top: 10px;
            margin-bottom: 10px;
        }
        .form-group{
            margin-right: 20px;
        }
    </style>
@endsection
@section('script')
    <script src="{{url('/js/blazy.min.js')}}" type="text/javascript"></script>
    <script src="{{url('js/jquery.growl.js')}}" type="text/javascript"></script>
    <script src="{{url('js/bootstrap-datepicker.min.js')}}" type="text/javascript"></script>

    <script>
                @if(request()->session()->has('sukses'))
        var msg = "{{request()->session()->get('sukses')}}";
        $.growl.notice({ title:"Sukses!", message: msg });
                @endif

                @if(request()->session()->has('gagal'))
        var msg = "{{request()->session()->get('gagal')}}";
        $.growl.error({ title:"Gagal!", message: msg });
                @endif
        var option = {
                clearBtn: true,
                autoclose: true,
                format: 'dd-mm-yyyy'
            };
        $('#dari_tanggal').datepicker(option);
        $('#sampai_tanggal').datepicker(option);
    </script>
    <script>
        var bLazy = new Blazy({
            breakpoints: [{
                width: 420 // Max-width
            }]
            , success: function(element){
                setTimeout(function(){
                    // We want to remove the loader gif now.
                    // First we find the parent container
                    // then we remove the "loading" class which holds the loader image

                }, 200);
            }
        });
    </script>
@endsection