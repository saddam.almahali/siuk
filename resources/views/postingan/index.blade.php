@extends('layouts.app')

@section('content')
    <div class="konten">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h4 class="panel-title">DAFTAR POSTINGAN KEGIATAN</h4>
                    </div>
                    <div class="panel-body">
                        <div class="">
                            <a href="{{url('/posting/baru')}}" class="btn btn-primary btn-sm">Tambah Posting</a>
                        </div>
                        <div class="page-header">
                            <form action="" class="form-inline">
                                <div class="form-group">
                                    <label>Dari Tanggal : </label>
                                    <input type="text" class="form-control" name="dari_tanggal" id="dari_tanggal">
                                </div>
                                <div class="form-group">
                                    <label>Sampai Tanggal : </label>
                                    <input type="text" class="form-control" name="sampai_tanggal" id="sampai_tanggal">
                                </div>
                                <div class="form-group">
                                    <label>Per Page : </label>
                                    <select name="per_page" class="form-control input-lg" id="per_page">
                                        <option value="5" selected>5</option>
                                        <option value="">10</option>
                                        <option value="">15</option>
                                    </select>
                                </div>
                                <div class="form-group pull-right">
                                    <input type="submit" class="btn btn-primary btn-sm" value="Filter">
                                    <input type="submit" class="btn btn-warning btn-sm" onclick="event.preventDefault(); window.history.back();" value="Reset">
                                </div>
                            </form>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                <th width="5%" style="text-align: center;">#</th>
                                <th width="30%" style="text-align: center;">Judul</th>
                                <th style="text-align: center;">Tanggal</th>
                                <th style="text-align: center;">Author</th>
                                <th style="text-align: center;">status</th>
                                <th style="text-align: center;">Opsi</th>

                                </thead>
                                <tbody>
                                <?php $i = 0; ?>
                                @forelse($data_posting as $posting)
                                <tr>
                                    <td align="center">{{$i+1}}</td>
                                    <td align="center">{{$posting->judul_posting}}</td>
                                    <td align="center">{{$posting->tanggal_posting}}</td>
                                    <td align="center">{{$posting->author}}</td>
                                    <td align="center">


                                        @if($posting->approve->status == 'waiting')
                                            <span class="label label-warning">Menunggu</span>
                                        @endif

                                        @if($posting->approve->status == 'approval')
                                            <span class="label label-info">Disetujui</span>
                                        @endif

                                        @if($posting->approve->status == 'reject')
                                            <span class="label label-danger">Ditolak</span>
                                        @endif

                                    </td>
                                    <td align="center">
                                        <a href="{{url('/posting/preview').'/'.$posting->id}}" title="preview" class="btn btn-primary btn-sm"><i class="fa fa-search"></i></a>
                                        <a href="{{url('/posting/edit').'/'.$posting->id}}" class="btn btn-info btn-sm" title="Edit Posting" {{auth()->user()->can('edit_postingan')? '' : 'disabled'}}><i class="fa fa-pencil"></i></a>
                                        <a href="{{url('/posting/hapus').'/'.$posting->id}}" onclick="if(confirm('Apakah Anda Yakin Akan Menghapus Posting Ini?')){return true;}else return false;" {{auth()->user()->can('hapus_postingan')? '' : 'disabled'}} title="Hapus Posting" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                                <?php $i++; ?>
                                @empty
                                    <tr>
                                        <td align="center" colspan="6">Tidak Ada Data Posting</td>
                                    </tr>
                                @endforelse
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('style')
    <link href="{{url('css/jquery.growl.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{url('css/bootstrap-datepicker3.min.css')}}" rel="stylesheet" type="text/css" />
    <style>
        span.month{
            color: #8d8d8d;
        }
        .konten {
            margin-top: 20px;
        }
        .konten > .row {
            margin-right: 20px;
            margin-left: 30px;
        }
        .row{
            margin-top: 10px;
            margin-bottom: 10px;
        }
        .form-group{
            margin-right: 20px;
        }
    </style>
@endsection
@section('script')
    <script src="{{url('js/jquery.growl.js')}}" type="text/javascript"></script>
    <script src="{{url('js/bootstrap-datepicker.min.js')}}" type="text/javascript"></script>

    <script>
                @if(request()->session()->has('sukses'))
        var msg = "{{request()->session()->get('sukses')}}";
        $.growl.notice({ title:"Sukses!", message: msg });
                @endif

                @if(request()->session()->has('gagal'))
        var msg = "{{request()->session()->get('gagal')}}";
        $.growl.error({ title:"Gagal!", message: msg });
        @endif
        var option = {
                        clearBtn: true,
                        autoclose: true,
                        format: 'dd-mm-yyyy'
                    };
        $('#dari_tanggal').datepicker(option);
        $('#sampai_tanggal').datepicker(option);
    </script>
@endsection