<!DOCTYPE html>
<html lang="en">

<head>

    <!-- Basic Page Needs -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="Offshore - Responsive HTML Template">
    <meta name="author" content="Ramdani">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Page Title -->
    <title> Offshore - Responsive Gas &amp; Oil Industry HTML Template </title>

    <!-- Favicon -->
    <link rel="icon" type="image/png" href="favicon.png">

    <!-- Mobile Specific Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Font Awesome CSS -->
    <link rel="stylesheet" href="fonts/font-awesome/css/font-awesome.min.css">

    <!-- Flex Slider -->
    <link rel="stylesheet" href="css/flexslider.css">

    <!-- Owl Carousel -->
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.min.css">

    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">

</head>

<body class="homepage">

<!-- Preloader -->
<div class="loader-wrapper">
    <div class="sk-cube-grid">
        <div class="sk-cube sk-cube1"></div>
        <div class="sk-cube sk-cube2"></div>
        <div class="sk-cube sk-cube3"></div>
        <div class="sk-cube sk-cube4"></div>
        <div class="sk-cube sk-cube5"></div>
        <div class="sk-cube sk-cube6"></div>
        <div class="sk-cube sk-cube7"></div>
        <div class="sk-cube sk-cube8"></div>
        <div class="sk-cube sk-cube9"></div>
    </div>
</div>

<div class="wrapper">
    <section class="login">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h4 class="panel-title text-center">
                            Login Ke Aplikasi
                        </h4>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div style="display: block; margin: 0 auto;">
                                    <center><img src="{{url('/images/logo.png')}}" width="300px" class="img img-responsive" alt=""></center>
                                </div>
                            </div>
                        </div>
                        <br />

                        <form action="{{url('/login')}}" id="login_form" method="post">
                            {{csrf_field()}}
                            <div class="form-group {{$errors->has('username') ? 'has-error' : ''}}">
                                <input type="text" class="form-control" placeholder="Username" name="username">
                                @if ($errors->has('username'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('username') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group {{$errors->has('password')? 'has-error':''}}">
                                <input type="password" class="form-control" placeholder="Password" name="password">
                                @if($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{$errors->first('password')}}</strong>
                                    </span>
                                @endif
                        </form>
                    </div>
                    <div class="panel-footer text-right">
                        <a href="{{url('/login')}}" class="btn btn-primary btn-sm " onclick="event.preventDefault();document.getElementById('login_form').submit();">Masuk</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<!-- Javascripts
–––––––––––––––––––––––––––––––––––––––––––––––––– -->

<!-- Jquery Library -->
<script src="js/jquery.min.js"></script>

<!-- Bootstrap core JavaScript -->
<script src="js/bootstrap.min.js"></script>

<!-- jQuery Flex Slider -->
<script src="js/jquery.flexslider-min.js"></script>

<!-- Owl Carousel -->
<script src="js/owl.carousel.min.js"></script>

<!-- Counter JS -->
<script src="js/waypoints.min.js"></script>
<script src="js/jquery.counterup.min.js"></script>

<!-- Back to top -->
<script src="js/back-to-top.js"></script>

<!-- Form Validation -->
<script src="js/validate.js"></script>

<!-- Subscribe Form JS -->
<script src="js/subscribe.js"></script>

<!-- Main JS -->
<script src="js/main.js"></script>

</body>

</html>
