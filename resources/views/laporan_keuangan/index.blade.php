@extends('layouts.app')

@section('content')
    <div class="konten">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title text-center">Laporan Keuangan</h3>
                    </div>
                    <div class="panel-body">
                        <div class="page-header">
                            <form action="" id="form-filter" method="get" class="form-inline">
                                <div class="form-group">
                                    <label>Dari Tanggal : </label>
                                    <input type="text" placeholder="Tanggal Awal" class="form-control" name="dari_tanggal" id="dari_tanggal" required autofocus>
                                </div>
                                <div class="form-group">
                                    <label>Sampai Tanggal : </label>
                                    <input type="text" placeholder="Tanggal Akhir" class="form-control" name="sampai_tanggal" id="sampai_tanggal" required autofocus>
                                </div>
                                <div class="form-group pull-right">
                                    <a href="" onclick="event.preventDefault();document.getElementById('form-filter').submit();" class="btn btn-primary btn-sm">Submit</a>
                                    <a class="btn btn-warning btn-sm" href="{{url('/manage/agenda')}}"> Reset</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('style')
    <link href="{{url('css/jquery.growl.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{url('css/bootstrap-datepicker3.min.css')}}" rel="stylesheet" type="text/css" />
    <style>
        span.month{
            color: #8d8d8d;
        }
        .konten {
            margin-top: 20px;
        }
        .konten > .row {
            margin-right: 20px;
            margin-left: 30px;
        }
        .row{
            margin-top: 10px;
            margin-bottom: 10px;
        }
        .form-group{
            margin-right: 20px;
        }
    </style>
@endsection
@section('script')
    <script src="{{url('js/jquery.growl.js')}}" type="text/javascript"></script>
    <script src="{{url('js/bootstrap-datepicker.min.js')}}" type="text/javascript"></script>

    <script>
                @if(request()->session()->has('sukses'))
        var msg = "{{request()->session()->get('sukses')}}";
        $.growl.notice({ title:"Sukses!", message: msg });
                @endif

                @if(request()->session()->has('gagal'))
        var msg = "{{request()->session()->get('gagal')}}";
        $.growl.error({ title:"Gagal!", message: msg });
                @endif
        var option = {
                clearBtn: true,
                autoclose: true,
                format: 'dd-mm-yyyy'
            };
        $('#dari_tanggal').datepicker(option);
        $('#sampai_tanggal').datepicker(option);
    </script>
@endsection