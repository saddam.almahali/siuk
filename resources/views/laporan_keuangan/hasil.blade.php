@extends('layouts.app')

@section('content')
    <div class="konten">
        <div class="row">
            <div class="col-md-12">
                <div class="panel">
                    <div class="panel-body">
                        <div class="page-header text-center">
                            <h3>LAPORAN KEUANGAN <br/> UKM SENI SEKOLAH TINGGI TEKNOLOGI GARUT <br /> Tahun {{date('Y', strtotime($sampai_tanggal))}}</h3>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <table id="header_laporan">
                                    <tr>
                                        <th width="200px">Dari Tanggal</th>
                                        <td width="20px" align="center">:</td>
                                        <td>{{$dari_tanggal}}</td>
                                    </tr>
                                    <tr>
                                        <th width="200px">Sampai Tanggal</th>
                                        <td width="20px" align="center">:</td>
                                        <td>{{$sampai_tanggal}}</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <thead>
                                        <tr>
                                            <th width="5%" style="text-align: center;">#</th>
                                            <th width="10%" style="text-align: center;">Tanggal</th>
                                            <th width="30%" style="text-align: center;">Uraian</th>
                                            <th width="15%" style="text-align: center;">Debet</th>
                                            <th width="15%" style="text-align: center;">Kredit</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php $i=0; $tanggal_last = ''; $total_masuk = 0; $total_keluar=0;?>
                                        @forelse($data_keuangan as $data)
                                            <tr>
                                                <td align="center">{{$i+1}}</td>
                                                <td align="center">{{$data->tanggal != $tanggal_last ? date('d-m-y', strtotime($data->tanggal)) : ''}}</td>
                                                <td align="left">
                                                    {{$data->keterangan}}
                                                </td>
                                                <td align="right">
                                                    @if($data->tipe=='masuk')
                                                        Rp. {{number_format($data->jumlah)}},-
                                                        <?php $total_masuk = $total_masuk+$data->jumlah; ?>
                                                    @else
                                                        -
                                                    @endif
                                                </td>
                                                <td align="right">
                                                    @if($data->tipe=='keluar')
                                                        Rp. {{number_format($data->jumlah)}},-
                                                        <?php $total_keluar = $total_keluar+$data->jumlah; ?>
                                                    @else
                                                        -
                                                    @endif
                                                </td>
                                            </tr>
                                            <?php $i++; $tanggal_last=$data->tanggal; ?>
                                        @empty
                                            <tr>
                                                <td colspan="5" align="center">Tidak Terdapat Transaksi Pada Rentang Tanggal</td>
                                            </tr>
                                        @endforelse
                                        </tbody>

                                    </table>
                                    <table class="pull-right" id="header_laporan">
                                        <tr>
                                            <th width="150px">Pendapatan</th>
                                            <td>:</td>
                                            <td align="right">Rp. {{number_format($total_masuk)}},-</td>
                                        </tr>
                                        <tr>
                                            <th width="150px">Pengeluaran</th>
                                            <td>:</td>
                                            <td align="right">Rp. {{number_format($total_keluar)}},-</td>
                                        </tr>
                                        <tr>
                                            <th width="150px">Pendapatan Bersih</th>
                                            <td>:</td>
                                            <td align="right">Rp. {{number_format($total_masuk-$total_keluar)}},-</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('style')
    <link href="{{url('css/jquery.growl.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{url('css/bootstrap-datepicker3.min.css')}}" rel="stylesheet" type="text/css" />
    <style>
        span.month{
            color: #8d8d8d;
        }
        .konten {
            margin-top: 20px;
        }
        .konten > .row {
            margin-right: 20px;
            margin-left: 30px;
        }
        .row{
            margin-top: 10px;
            margin-bottom: 10px;
        }
        .form-group{
            margin-right: 20px;
        }

        #header_laporan{

            border-spacing: 10px;
            border-collapse: separate;

        }
    </style>
@endsection
@section('script')
    <script src="{{url('js/jquery.growl.js')}}" type="text/javascript"></script>
    <script src="{{url('js/bootstrap-datepicker.min.js')}}" type="text/javascript"></script>

    <script>
                @if(request()->session()->has('sukses'))
        var msg = "{{request()->session()->get('sukses')}}";
        $.growl.notice({ title:"Sukses!", message: msg });
                @endif

                @if(request()->session()->has('gagal'))
        var msg = "{{request()->session()->get('gagal')}}";
        $.growl.error({ title:"Gagal!", message: msg });
                @endif
        var option = {
                clearBtn: true,
                autoclose: true,
                format: 'dd-mm-yyyy'
            };
        $('#dari_tanggal').datepicker(option);
        $('#sampai_tanggal').datepicker(option);
    </script>
@endsection