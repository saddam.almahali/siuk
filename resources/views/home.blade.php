@extends('layouts.app')

@section('content')

    @if(auth()->user()->hasRole('admin'))
        <div class="konten">
            <div class="row">
                <div class="col-md-6 text-align">
                    <div class="row">
                        <div id="widget-user" class="col-md-5">
                            <div class="row">
                                <div class="col-md-5">
                                    <img src="{{url('/images/icons/user.svg')}}" width="100px" alt="img">
                                </div>
                                <div class="col-md-6">
                                    <div>
                                        <h4>ANGGOTA</h4>
                                    </div>
                                    <div>
                                        <h4>{{$jumlah_anggota}} Orang</h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="widget-user" class="col-md-5">
                            <div class="row">
                                <div class="col-md-5">
                                    <img src="{{url('/images/icons/money.svg')}}" width="100px" alt="img">
                                </div>
                                <div class="col-md-6">
                                    <div>
                                        <h4>KAS</h4>
                                    </div>
                                    <div>
                                        <h4>Rp. {{number_format($uang_kas)}},-</h4>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="row">
                        <div id="widget-user" class="col-md-5">
                            <div class="row">
                                <div class="col-md-5">
                                    <img src="{{url('/images/icons/agenda.svg')}}" width="100px" alt="img">
                                </div>
                                <div class="col-md-6">
                                    <div>
                                        <h4>AGENDA</h4>
                                    </div>
                                    <div>
                                        <h4>{{$jumlah_agenda}} Acara</h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="widget-user" class="col-md-5">
                            <div class="row">
                                <div class="col-md-5">
                                    <img src="{{url('/images/icons/articles.svg')}}" width="100px" alt="img">
                                </div>
                                <div class="col-md-6">
                                    <div>
                                        <h4>ARTIKEL</h4>
                                    </div>
                                    <div>
                                        <h4>{{$jumlah_artikel}} Posting</h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Recent User</h3>
                                </div>
                                <div class="panel-body">
                                    <table  class="table table-bordered">
                                        <thead>
                                        <tr>
                                            <th style="text-align: center;">NPM</th>
                                            <th style="text-align: center;">Nama</th>
                                            <th style="text-align: center;">Opsi</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @forelse($recent_anggota as $anggota)
                                            <tr>
                                                <td align="center">{{$anggota->npm}}</td>
                                                <td align="center">{{$anggota->nama_lengkap}}</td>
                                                <td align="center">
                                                    <a href="{{url('/admin/anggota/view/7')}}" title="Lihat Detile Anggota"><i class="fa fa-search"></i></a>
                                                </td>
                                            </tr>
                                        @empty
                                            <tr>
                                                <td colspan="3" align="center">Tidak Ada Data Anggota</td>
                                            </tr>
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    @endif

    @if(auth()->user()->hasRole('user'))
        <div class="konten">
            <div class="row">
                <div id="widget-user" class="col-md-8 col-md-offset-2">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <div class="page-header">
                                <h3 class="text-center">Selamat Datang di Sistem Informasi UKM</h3>
                            </div>
                            <p class="text-center">
                                Sistem ini Melayani Manajemen Konten dan Kegiatan Unit Kegiatan Mahasiswa Sekolah Tinggi Teknologi Garut
                            </p>
                            <br />
                            <p class="text-center">
                                <b>Hormat Kami</b> <br />
                                <b>UKM Seni STT Garut</b>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif

@endsection

@section('style')
    <style>
        #widget-user{

            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
            margin-top: 10px;
            padding: 10px;
        }

        .konten {
            margin-top: 20px;
            margin-right: 50px;
            margin-left: 50px;
        }


        .panel {
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
            margin-top: 10px;
        }

    </style>
@endsection