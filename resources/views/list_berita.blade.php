@extends('layouts.app')

@section('content')
    <!-- Page Title -->
    <div class="page-title text-center">
        <h2 class="title"> Berita UKM </h2>
        <p class="description light"> Berisikan Aktifitas UKM Seni.
            <br> Sekolah Tinggi Teknologi Garut. </p>
    </div>
    <!-- Page Title -->
    <!-- Breadcrumbs -->
    <div class="breadcrumbs">
        <div class="container">
            <span class="parent"> <i class="fa fa-home"></i> <a href="{{url('/')}}"> Home </a> </span>
            <i class="fa fa-chevron-right"></i>
            <span class="child"> Blog </span>
        </div>
    </div>

    <div class="container">
        <div class="row">

            <div class="col-md-9 blog-grid">
                <section class="blog-services">
                    <div class="row news">
                        <?php $flag = 0; ?>
                        @forelse($data_berita as $berita)
                            <div class="col-sm-6">
                                <div class="blog-img-box">
                                    <div class="blog-date"> <span class="month">{{date('M', strtotime($berita->tanggal_posting))}} </span> <span class="date"> {{date('d', strtotime($berita->tanggal_posting))}}</span> </div>
                                    <a class="hover-effect" href="{{url('/berita/get').'/'.$berita->id}}">
                                        <img data-src="{{url('/uploads/get').'/'.$berita->gambar()->first()->gambar}}" class="b-lazy" alt="Fuel" />
                                    </a>
                                </div>
                                <div class="blog-content">
                                    <h3><a href="{{url('/berita/get').'/'.$berita->id}}"> {{$berita->judul_posting}} </a> </h3>
                                    <p>By <a href="#">{{$berita->author}}</a></p>
                                </div>
                            </div>

                            @if($flag == 1)
                                <div class="clearfix spacer-60"> </div>
                                <?php $flag =0; ?>
                            @else
                                <?php $flag++; ?>
                            @endif

                        @empty
                            <center>TIDAK ADA BERITA</center>
                        @endforelse

                    </div>
                </section>
            </div>

            <div class="col-md-3 sidebar">
                <div class="login-box">
                    <button class="btn btn-info btn-block" onclick="location.href='/login'"><i class="fa fa-key"></i>&nbsp;&nbsp;LOGIN</button>
                    <button class="btn btn-success btn-block" onclick="location.href='/daftar_anggota'"><i class="fa fa-calendar-o"></i>&nbsp;&nbsp;DAFTAR</button>

                </div>
                <br />
                <div class="plubication-downloads">
                    <h2 class="publish"><i class="fa fa-calendar"></i>&nbsp;&nbsp;AGENDA</h2>

                    <ul class="download-list">
                        @forelse($data_agenda as $agenda)
                            <li><a href="{{url('/manage/agenda/detile').'/'.$agenda->id}}"> {{$agenda->nama_kegiatan}} </a> <span>{{date('d-m-Y', strtotime($agenda->tanggal))}}</span></li>
                        @empty
                            <li class="text-center">Tidak Terdapat Anggota</li>
                        @endforelse
                    </ul>
                </div>
            </div>

        </div>
    </div>
@endsection

@section('style')
    <style>
        .b-lazy {
            opacity:0;
            transform: scale(3);
            transition: all 500ms;
        }
        .b-loaded {
            opacity:1;
            transform: scale(1);
        }
        #widget-user{

            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
            margin-top: 10px;
            margin-left: 10px;
            padding: 10px;
        }

        .konten {
            margin-top: 20px;
            margin-right: 50px;
            margin-left: 50px;
        }

        #widget-user.img {
            width: 100px;
        }

        .panel {
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
            margin-top: 10px;
        }

    </style>
@endsection
@section('script')
    <script src="{{url('/js/blazy.min.js')}}" type="text/javascript"></script>
    <script>
        var bLazy = new Blazy({
            breakpoints: [{
                width: 420 // Max-width
            }]
            , success: function(element){
                setTimeout(function(){
                    // We want to remove the loader gif now.
                    // First we find the parent container
                    // then we remove the "loading" class which holds the loader image

                }, 200);
            }
        });
    </script>
@endsection