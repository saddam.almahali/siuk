<!-- Main Navigation Section -->
<nav id="navbar" class="collapse navbar-collapse main-menu">
    <div class="container">
        @if(auth()->guest())
            <ul class="main-menu text-center">
                <li class="text-justify {{url()->full() == url('/') ? 'active' :''}}"> <a href="{{url('/')}}"> Home </a></li>
                <li class="text-justify" {{url()->full() == url('/berita') ? 'active' :''}}><a href="{{url('/berita')}}"> Berita </a> </li>
                <li class="text-justify" {{url()->full() == url('/daftar_anggota') ? 'active' :''}}><a href="{{url('/daftar_anggota')}}"> Daftar Anggota </a> </li>
                <li class="text-justify" {{url()->full() == url('/list_agenda') ? 'active' :''}}><a href="{{url('/list_agenda')}}"> List Agenda </a> </li>
                <li class="text-justify"><a href="{{url('/tentang')}}"> TENTANG </a> </li>
            </ul>
        @else
            @if(auth()->user()->hasRole('admin'))
                <ul class="main-menu text-center">
                    <li class="text-justify {{url()->full() == url('/home') ? 'active' : ''}}">
                        <a href="{{url('/home')}}"> <i class="fa fa-dashboard"></i>&nbsp; &nbsp;Dashboard </a>
                    </li>

                    <li class="dropdown text-justify"> <a href="#" data-toggle="dropdown"> <i class="fa fa-file-text"></i>&nbsp;&nbsp; Posting
                            <i class="fa fa-chevron-down dropdown-toggle"> </i>  </a>
                        <ul>
                            <li> <a href="{{url('/posting')}}"> List Posting </a> </li>
                            <li> <a href="{{url('/posting/baru')}}"> Tambah Postingan </a> </li>
                        </ul>
                    </li>
                    <li class="dropdown text-justify"> <a href="#" data-toggle="dropdown"> <i class="fa fa-money"></i>&nbsp;&nbsp; Keuangan
                            <i class="fa fa-chevron-down dropdown-toggle"> </i>  </a>
                        <ul>
                            <li> <a href="{{url('/admin/keuangan')}}"> Data Keuangan </a> </li>
                            <li> <a href="{{url('/admin/laporan_keuangan')}}"> Laporan Keuangan </a> </li>
                        </ul>
                    </li>
                    <li class="dropdown text-justify"> <a href="#" data-toggle="dropdown"> <i class="fa fa-calendar"></i>&nbsp;&nbsp; Agenda
                            <i class="fa fa-chevron-down dropdown-toggle"> </i>  </a>
                        <ul>
                            <li> <a href="{{url('/manage/agenda')}}"> List Agenda </a> </li>
                            <li> <a href="{{url('/manage/agenda/baru')}}"> Tambah Agenda </a> </li>
                        </ul>
                    </li>
                    <li class="dropdown text-justify"> <a href="#" data-toggle="dropdown"> <i class="fa fa-users"></i>&nbsp;&nbsp; Keanggotaan
                            <i class="fa fa-chevron-down dropdown-toggle"> </i>  </a>
                        <ul>
                            <li> <a href="{{url('/admin/pendaftaran')}}"> Pendaftaran </a> </li>
                            <li> <a href="{{url('/admin/jenis_kegiatan')}}"> Jenis Kegiatan </a> </li>
                            <li> <a href="{{url('/admin/anggota')}}"> Anggota </a> </li>
                        </ul>
                    </li>
                    <li class="dropdown text-justify"> <a href="#" data-toggle="dropdown"> <i class="fa fa-gear"></i>&nbsp;&nbsp; Preferences
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-chevron-down dropdown-toggle"></i></a>
                        <ul>
                            <li> <a href="{{url('/admin/preferences/role')}}"> Role</a> </li>
                            <li> <a href="{{url('/admin/preferences/hakakses')}}"> Hak Akses </a> </li>

                        </ul>
                    </li>
                </ul>
            @endif

            @if(auth()->user()->hasRole('user'))
                    <ul class="main-menu text-center">
                        <li class="text-justify {{url()->full() == url('/home') ? 'active' : ''}}">
                            <a href="{{url('/home')}}"> <i class="fa fa-dashboard"></i>&nbsp; &nbsp;Dashboard </a>
                        </li>

                        <li class="dropdown text-justify"> <a href="#" data-toggle="dropdown"> <i class="fa fa-file-text"></i>&nbsp;&nbsp; Posting
                                <i class="fa fa-chevron-down dropdown-toggle"> </i>  </a>
                            <ul>
                                <li> <a href="{{url('/posting')}}"> List Posting </a> </li>
                                <li> <a href="{{url('/posting/baru')}}"> Tambah Postingan </a> </li>
                            </ul>
                        </li>
                        <li class="dropdown text-justify"> <a href="#" data-toggle="dropdown"> <i class="fa fa-calendar"></i>&nbsp;&nbsp; Agenda
                                </a>
                            <ul>
                                <li> <a href="{{url('/manage/agenda')}}"> List Agenda </a> </li>
                                <li> <a href="{{url('/manage/agenda/baru')}}"> Tambah Agenda </a> </li>
                            </ul>
                        </li>
                    </ul>
            @endif
        @endif
    </div>
</nav>