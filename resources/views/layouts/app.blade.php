<!DOCTYPE html>
<html lang="en">

<head>

    <!-- Basic Page Needs -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="Offshore - Responsive HTML Template">
    <meta name="author" content="Ramdani">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Page Title -->
    <title> Sistem Informasi UKM Seni STT Garut</title>

    <!-- Favicon -->
    <link rel="icon" type="image/png" href="{{url('favicon.png')}}">

    <!-- Mobile Specific Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap core CSS -->
    <link href="{{url('css/bootstrap.min.css')}}" rel="stylesheet">

    <!-- Font Awesome CSS -->
    <link rel="stylesheet" href="{{url('fonts/font-awesome/css/font-awesome.min.css')}}">

    <!-- Flex Slider -->
    <link rel="stylesheet" href="{{url('css/flexslider.css')}}">

    <!-- Owl Carousel -->
    <link rel="stylesheet" href="{{url('css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{url('css/owl.theme.min.css')}}">

    <!-- Custom styles for this template -->
    <link href="{{url('css/style.css')}}" rel="stylesheet">
    @yield('style')
</head>

<body class="homepage">

<!-- Preloader -->
<div class="loader-wrapper">
    <div class="sk-cube-grid">
        <div class="sk-cube sk-cube1"></div>
        <div class="sk-cube sk-cube2"></div>
        <div class="sk-cube sk-cube3"></div>
        <div class="sk-cube sk-cube4"></div>
        <div class="sk-cube sk-cube5"></div>
        <div class="sk-cube sk-cube6"></div>
        <div class="sk-cube sk-cube7"></div>
        <div class="sk-cube sk-cube8"></div>
        <div class="sk-cube sk-cube9"></div>
    </div>
</div>

<!-- Page Wrapper -->
<div class="wrapper">

    <!-- Header Section -->
    <header>
        <div class="header-area">

            @include('layouts.topinfo')

            @include('layouts.topnav')
        </div>
    </header>
    <!-- Header Section -->

    <!-- Main Content Section -->
    <main class="main">
        @yield('content')
    </main>
    <!-- Main Content Section -->


    @include('layouts.footer')

    <!-- back-to-top link -->
    <a href="#0" class="cd-top"> Top </a>

</div>

<!-- Page Wrapper
–––––––––––––––––––––––––––––––––––––––––––––––––– -->

<!-- Javascripts
–––––––––––––––––––––––––––––––––––––––––––––––––– -->

<!-- Jquery Library -->
<script src="{{url('js/jquery.min.js')}}"></script>

<!-- Bootstrap core JavaScript -->
<script src="{{url('js/bootstrap.min.js')}}"></script>

<!-- jQuery Flex Slider -->
<script src="{{url('js/jquery.flexslider-min.js')}}"></script>

<!-- Owl Carousel -->
<script src="{{url('js/owl.carousel.min.js')}}"></script>

<!-- Counter JS -->
<script src="{{url('js/waypoints.min.js')}}"></script>
<script src="{{url('js/jquery.counterup.min.js')}}"></script>

<!-- Back to top -->
<script src="{{url('js/back-to-top.js')}}"></script>

<!-- Form Validation -->
<script src="{{url('js/validate.js')}}"></script>

<!-- Subscribe Form JS -->
<script src="{{url('js/subscribe.js')}}"></script>

<!-- Main JS -->
<script src="{{url('js/main.js')}}"></script>
@yield('script')
</body>

</html>
