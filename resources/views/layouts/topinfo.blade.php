<!-- Top Contact Info -->
<div class="row logo-top-info">
    <div class="container">
        <div class="col-md-3 logo">
            <!-- Main Logo -->
            <a href="{{url('/')}}"><img src="{{url('images/logo.png')}}" alt="Offshore Logo" /></a>
            <!-- Responsive Toggle Menu -->
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar">
                <span class="sr-only"> Main Menu </span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div class="col-md-9 top-info-social">
            <div class="pull-right">
                <div class="top-info">

                    @if(auth('web')->check())
                        <div class="email">
                            <ul class="nav navbar-nav navbar-right">
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-user"></i>&nbsp;&nbsp;{{auth()->user()->name}} <span class="caret"></span></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="{{url('/logout')}}" onclick="event.preventDefault(); document.getElementById('logout').submit();"><i class="fa fa-sign-out"></i>&nbsp;&nbsp; Logout</a></li>
                                    </ul>
                                    <form action="{{url('/logout')}}" method="post" id="logout">
                                        {{csrf_field()}}
                                    </form>
                                </li>
                            </ul>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>