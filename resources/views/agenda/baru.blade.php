@extends('layouts.app')

@section('content')
    <div class="konten">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h4 class="panel-title">Tambah Agenda Kegiatan</h4>
                    </div>
                    <div class="panel-body">
                        <form action="{{url('/manage/agenda/simpan')}}" method="post">
                            {{csrf_field()}}
                            <div class="form-group {{$errors->has('jenis') ? 'has-error' : ''}}">
                                <select name="jenis" class="form-control input-sm">
                                    <option value="" selected disabled>Pilih Jenis</option>
                                    @foreach($data_jenis as $jenis)
                                        <option value="{{$jenis->id}}">{{strtoupper($jenis->nama_jenis)}}</option>
                                    @endforeach
                                </select>
                                @if($errors->has('jenis'))
                                    <span class="help-block">
                                        <strong>{{$errors->first('jenis')}}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group {{$errors->has('nama_kegiatan') ? 'has-error' : ''}}">
                                <input type="text" class="form-control input-sm" name="nama_kegiatan" value="{{old('nama_kegiatan')}}" placeholder="Nama Kegiatan">
                                @if($errors->has('nama_kegiatan'))
                                    <span class="help-block">
                                        <strong>{{$errors->first('nama_kegiatan')}}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group {{$errors->has('tanggal_waktu') ? 'has-error' : ''}}">
                                <input type="text" id="tanggal_waktu" name="tanggal_waktu" class="form-control input-sm" value="{{old('tanggal_waktu')}}" name="tanggal" placeholder="Tanggal dan Waktu Pelaksanaan">
                                @if($errors->has('tanggal_waktu'))
                                    <span class="help-block">
                                        <strong>{{$errors->first('tanggal_waktu')}}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group {{$errors->has('sifat') ? 'has-error' : ''}}">
                                <select name="sifat" class="form-control input-sm">
                                    <option value="" selected disabled>Pilih Sifat</option>
                                    <option value="biasa">Biasa</option>
                                    <option value="wajib">Penting</option>
                                    <option value="wajib">Wajib</option>
                                </select>
                                @if($errors->has('sifat'))
                                    <span class="help-block">
                                        <strong>{{$errors->first('sifat')}}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group {{$errors->has('agenda') ? 'has-error' : ''}}">
                                <textarea name="agenda" id="agenda" class="form-control input-sm" rows="3" placeholder="Agenda Kegiatan">{{old('agenda')}}</textarea>
                                @if($errors->has('agenda'))
                                    <span class="help-block">
                                        <strong>{{$errors->first('agenda')}}</strong>
                                    </span>
                                @endif
                            </div>

                            <input type="submit" class="btn btn-primary btn-sm pull-right" style="margin-left:15px; width:100px;" value="Simpan">
                            <input type="submit" class="btn btn-danger btn-sm pull-right" style="width: 100px" onclick="event.preventDefault(); history.back();" value="Batal">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('style')
    <link href="{{url('css/bootstrap-datetimepicker.min.css')}}" rel="stylesheet" type="text/css" />
    <style>
        span.month{
            color: #8d8d8d;
        }
        .konten {
            margin-top: 20px;
        }
        .konten > .row {
            margin-right: 20px;
            margin-left: 30px;
        }
        .row{
            margin-top: 10px;
            margin-bottom: 10px;
        }
    </style>
@endsection

@section('script')
    <script src="{{url('js/moment.js')}}" type="text/javascript"></script>
    <script src="{{url('js/bootstrap-datetimepicker.min.js')}}" type="text/javascript"></script>
    <script>

        var option = {

        };
        $('#tanggal_waktu').datetimepicker(option);
    </script>
@endsection