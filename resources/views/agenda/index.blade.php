@extends('layouts.app')

@section('content')
    <div class="konten">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">DAFTAR POSTINGAN KEGIATAN</h3>
                    </div>
                    <div class="panel-body">
                        <div class="">
                            <a href="{{url('/manage/agenda/baru')}}" class="btn btn-primary btn-sm">Tambah Agenda</a>
                        </div>
                        <div class="page-header">
                            <form action="" id="form-filter" method="get" class="form-inline">
                                <div class="form-group">
                                    <label>Dari Tanggal : </label>
                                    <input type="text" class="form-control" name="dari_tanggal" id="dari_tanggal">
                                </div>
                                <div class="form-group">
                                    <label>Sampai Tanggal : </label>
                                    <input type="text" class="form-control" name="sampai_tanggal" id="sampai_tanggal">
                                </div>
                                <div class="form-group">
                                    <label>Per Page : </label>
                                    <select name="per_page" class="form-control input-lg" id="per_page">
                                        <option value="5" selected>5</option>
                                        <option value="">10</option>
                                        <option value="">15</option>
                                    </select>
                                </div>
                                <div class="form-group pull-right">
                                    <a href="" onclick="event.preventDefault();document.getElementById('form-filter').submit();" class="btn btn-primary btn-sm">Submit</a>
                                    <a class="btn btn-warning btn-sm" href="{{url('/manage/agenda')}}"> Reset</a>
                                </div>
                            </form>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                <th width="5%" style="text-align: center;">#</th>
                                <th width="30%" style="text-align: center;">Kegiatan</th>
                                <th style="text-align: center;">Tanggal</th>
                                <th style="text-align: center;">Sifat</th>
                                <th style="text-align: center;">Opsi</th>
                                </thead>
                                <tbody>
                                <?php $i = 0; ?>
                                    @forelse($data_agenda as $agenda)
                                        <tr>
                                            <td align="center" style="vertical-align: middle;">{{$i+1}}</td>
                                            <td style="vertical-align: middle;">{{$agenda->nama_kegiatan}}</td>
                                            <td align="center" style="vertical-align: middle;">{{date('d-m-Y', strtotime($agenda->tanggal))}}</td>
                                            <td align="center" style="vertical-align: middle;">{{$agenda->sifat}}</td>
                                            <td align="center" style="vertical-align: middle;">
                                                <a href="{{url('/manage/agenda/detile').'/'.$agenda->id }}" class="btn btn-info btn-sm" title="Detail Agenda"><i class="fa fa-search"></i></a>
                                                <a href="{{url('/manage/agenda/ubah').'/'.$agenda->id }}" class="btn btn-warning btn-sm" title="Ubah Agenda"><i class="fa fa-calendar"></i></a>
                                                <a href="{{url('/manage/agenda/hapus').'/'.$agenda->id }}" onclick="if(confirm('Apakah Anda Yakin Akan Menghapus Agenda?')){return true}else return false;" class="btn btn-danger btn-sm" title="Hapus Agenda"><i class="fa fa-trash"></i></a>
                                            </td>
                                        </tr>
                                        <?php $i++; ?>
                                    @empty
                                        <tr>
                                            <td colspan="5" align="center">Tidak Terdapat Agenda</td>
                                        </tr>
                                    @endforelse
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('style')
    <link href="{{url('css/jquery.growl.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{url('css/bootstrap-datepicker3.min.css')}}" rel="stylesheet" type="text/css" />
    <style>
        span.month{
            color: #8d8d8d;
        }
        .konten {
            margin-top: 20px;
        }
        .konten > .row {
            margin-right: 20px;
            margin-left: 30px;
        }
        .row{
            margin-top: 10px;
            margin-bottom: 10px;
        }
        .form-group{
            margin-right: 20px;
        }
    </style>
@endsection
@section('script')
    <script src="{{url('js/jquery.growl.js')}}" type="text/javascript"></script>
    <script src="{{url('js/bootstrap-datepicker.min.js')}}" type="text/javascript"></script>

    <script>
                @if(request()->session()->has('sukses'))
        var msg = "{{request()->session()->get('sukses')}}";
        $.growl.notice({ title:"Sukses!", message: msg });
                @endif

                @if(request()->session()->has('gagal'))
        var msg = "{{request()->session()->get('gagal')}}";
        $.growl.error({ title:"Gagal!", message: msg });
                @endif
        var option = {
                clearBtn: true,
                autoclose: true,
                format: 'dd-mm-yyyy'
            };
        $('#dari_tanggal').datepicker(option);
        $('#sampai_tanggal').datepicker(option);
    </script>
@endsection