@extends('layouts.app')

@section('content')
    <div class="konten">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            Detile Agenda Kegiatan

                        </h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-6">
                                <table class="table table-borderless">
                                    <tr>
                                        <th>Jenis</th>
                                        <td>:</td>
                                        <td>{{$agenda->kegiatan->nama_jenis}}</td>
                                    </tr>
                                    <tr>
                                        <th>Nama</th>
                                        <td>:</td>
                                        <td>{{$agenda->nama_kegiatan}}</td>
                                    </tr>
                                    <tr>
                                        <th>Tanggal</th>
                                        <td>:</td>
                                        <td>{{$agenda->tanggal}}</td>
                                    </tr>
                                    <tr>
                                        <th>Waktu</th>
                                        <td>:</td>
                                        <td>{{date('H:i', strtotime($agenda->waktu))}}</td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-md-6">
                                <table class="table table-borderless">
                                    @if($agenda->sifat == 'biasa')
                                        <tr>
                                            <th>Sifat</th>
                                            <td>:</td>
                                            <td><span class="label label-info">{{$agenda->sifat}}</span></td>
                                        </tr>
                                    @endif
                                    @if($agenda->sifat == 'penting')
                                        <tr>
                                            <th>Sifat</th>
                                            <td>:</td>
                                            <td><span class="label label-warning">{{$agenda->sifat}}</span></td>
                                        </tr>
                                    @endif
                                    @if($agenda->sifat == 'wajib')
                                        <tr>
                                            <th>Sifat</th>
                                            <td>:</td>
                                            <td><span class="label label-danger">{{$agenda->sifat}}</span></td>
                                        </tr>
                                    @endif
                                    <tr>
                                        <th>Agenda</th>
                                        <td>:</td>
                                        <td>{{$agenda->agenda}}</td>
                                    </tr>
                                    <tr>
                                        <th>Pembuat</th>
                                        <td>:</td>
                                        <td>{{$agenda->pembuat}}</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="pull-right">
                            <a href="#" onclick="event.preventDefault(); window.history.back()" class="btn bt-warning btn-sm"><i class="fa fa-arrow-left"></i>&nbsp;&nbsp;Kembali</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('style')
    <link href="{{url('css/bootstrap-datetimepicker.min.css')}}" rel="stylesheet" type="text/css" />
    <style>
        span.month{
            color: #8d8d8d;
        }
        .konten {
            margin-top: 20px;
        }
        .konten > .row {
            margin-right: 20px;
            margin-left: 30px;
        }
        .row{
            margin-top: 10px;
            margin-bottom: 10px;
        }

        .table-borderless > tbody > tr > td,
        .table-borderless > tbody > tr > th,
        .table-borderless > tfoot > tr > td,
        .table-borderless > tfoot > tr > th,
        .table-borderless > thead > tr > td,
        .table-borderless > thead > tr > th {
            border: none;
        }
    </style>
@endsection

@section('script')
    <script src="{{url('js/moment.js')}}" type="text/javascript"></script>
    <script src="{{url('js/bootstrap-datetimepicker.min.js')}}" type="text/javascript"></script>\
@endsection