@extends('layouts.app')
@section('content')
    <section class="home-publications">
        <div class="container">
            <div class="row publications">
                <div class="col-md-7 col-sm-6">
                    <div class="panel-group" id="accordion">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">PROFIL</a>
                                </h4>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <p>
                                        UKM Seni dan Budaya STT Garut Berdiri mulai tahun 2012 sebagai organisasi intra kampus di STT Garut yang bernaung di bawah Yayasan Al-Musaddadiyah. Sebelumnya UKM tersebut adalah sebuah komunitas seni yang hanya berkesenian melalui komunitas Sunday Expression artinya setiap kegiatan kesenian dilakukan pada hari senin dari mulai pagi hari sampai selesai. Kegiatan tersebut terus berjalan hingga akhirnya pihak kampus memberikan ijin resmi untuk menjadikan komunitas tersebut sebagai organisasi intra kampus yang legal. STT Garut beralamat di Jl. Mayor Syamsu No. 1 Desa Jayaraga Kecamatan Tarogong Kidul, Jawa Barat 44151. Saat ini di ukm seni dan budaya STT Garut terdapat beberapa kategori yang terdiri dari seni musik, seni teater, stand up comedy, puisi, dan fotografi.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">VISI </a> </h4>
                            </div>
                            <div id="collapseTwo" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p>
                                    <ul>
                                        <li>UKM SENI DAN BUDAYA  memberikan kesempatan kepada mahasiswa/i untuk mengembangkan bakat, dan minat secara optimal dan menumbuhkan rasa percaya diri kepada setiap individu mahasiswa/i untuk mengekspresikan diri sesuai bakat dan minat.</li>

                                    </ul>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">MISI </a> </h4>
                            </div>
                            <div id="collapseThree" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p>
                                    <ol>
                                        <li>UKM SENI DAN BUDAYA berjuang mempersatukan Mahasiswa, STT Garut melalui kaderisasi Intelektual sehingga berkembang sebagai agen perubahan kearah yang lebih baik dengan di dasari nilai-nilai kekeluargaan.</li>
                                        <li>Menjadikan kampus tidak hanya sebagai tempat belajar namun juga sebagai tempat atau sarana untuk mengekspresikan diri secara positif.</li>
                                    </ol>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-5 col-sm-6">
                    <div class="login-box">
                        <button class="btn btn-info btn-block" onclick="location.href='/login'"><i class="fa fa-key"></i>&nbsp;&nbsp;LOGIN</button>
                        <button class="btn btn-success btn-block" onclick="location.href='/daftar_anggota'"><i class="fa fa-calendar-o"></i>&nbsp;&nbsp;DAFTAR</button>

                    </div>
                    <br />
                </div>

                <div class="col-md-5 col-sm-6">
                    <div class="plubication-downloads">
                        <h2 class="publish"><i class="fa fa-calendar"></i>&nbsp;&nbsp;AGENDA</h2>

                        <ul class="download-list">
                            @forelse($data_agenda as $agenda)
                                <li><a href="{{url('/manage/agenda/detile').'/'.$agenda->id}}"> {{$agenda->nama_kegiatan}} </a> <span>{{date('d-m-Y', strtotime($agenda->tanggal))}}</span></li>
                            @empty
                                <li class="text-center">Tidak Terdapat Anggota</li>
                            @endforelse
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="home-news">
        <div class="container">
            <div class="section-title text-center">
                <h2 class="subtitle-blog title-2"> BERITA UKM </h2>
                <div class="spacer-50"> </div>
            </div>
            <div class="row news">
                @forelse($data_posting as $posting)
                <div class="col-md-4">
                    <div class="blog-img-box">
                        <div class="blog-date"> <span class="month">{{date('M', strtotime($posting->tanggal_posting))}} </span> <span class="date"> {{date('d', strtotime($posting->tanggal_posting))}}</span> </div>
                        <a class="hover-effect" href="{{url('/berita/get').'/'.$posting->id}}">
                            <img data-src="{{url('/uploads/get').'/'.$posting->gambar()->first()->gambar}}" class="b-lazy" alt="Fuel" />
                        </a>
                    </div>
                    <div class="blog-content">
                        <h3><a href="{{url('/berita/get').'/'.$posting->id}}"> {{$posting->judul_posting}} </a> </h3>
                        <p>By <a href="#">{{$posting->author}}</a> </p>
                    </div>
                </div>
                @empty
                    <div class="text-center">Tidak Terdapat Berita</div>
                @endforelse
            </div>
            <div class="blog-btn text-center">
                <a href="{{url('/berita')}}" class="btn btn-primary" role="button">BERITA LAIN</a>
            </div>
        </div>
    </section>
@endsection
@section('style')
    <style>
        .b-lazy {
            opacity:0;
            transform: scale(3);
            transition: all 500ms;
        }
        .b-loaded {
            opacity:1;
            transform: scale(1);
        }

    </style>
@endsection
@section('script')
    <script src="{{url('/js/blazy.min.js')}}" type="text/javascript"></script>
    <script>
        var bLazy = new Blazy({
            breakpoints: [{
                width: 420 // Max-width
            }]
            , success: function(element){
                setTimeout(function(){
                    // We want to remove the loader gif now.
                    // First we find the parent container
                    // then we remove the "loading" class which holds the loader image

                }, 200);
            }
        });
    </script>
@endsection