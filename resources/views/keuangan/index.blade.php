@extends('layouts.app')

@section('content')
    <div class="konten">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">DAFTAR KEUANGAN UKM SENI</h3>
                    </div>
                    <div class="panel-body">
                        <div class="page-header">
                            <a href="{{url('/admin/keuangan/tambah_pemasukan')}}" class="btn btn-primary btn-sm">Tambah Pemasukan</a>
                            <a href="{{url('/admin/keuangan/tambah_pengeluaran')}}" class="btn btn-warning btn-sm">Tambah Pengeluaran</a>
                            <div class="pull-right">
                                <h3><b>Kas Rp. {{$uang_kas}},-</b></h3>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <thead>
                                        <th width="30%" style="text-align: center;">KETERANGAN</th>
                                        <th style="text-align: center;">TANGGAL</th>
                                        <th style="text-align: center; width: 5%;">STATUS</th>
                                        <th width="15%" style="text-align: center;">DEBET</th>
                                        <th width="15%" style="text-align: center;">KREDIT</th>
                                        <th style="text-align: center;">OPSI</th>

                                        </thead>
                                        <tbody>
                                        <?php $i = 0; ?>
                                        @forelse($data_keuangan as $data)
                                            <tr>
                                                <td style="vertical-align: middle;" align="left">{{$data->keterangan}}</td>
                                                <td style="vertical-align: middle;" align="center">{{date('d-m-Y', strtotime($data->tanggal))}}</td>
                                                <td style="vertical-align: middle;" align="center">{!! $data->tipe == 'masuk' ? '<span class="label label-info">masuk</span>': '<span class="label label-warning">keluar</span>' !!}</td>
                                                <td style="vertical-align: middle;" align="right">Rp. {{number_format($data->tipe == 'masuk' ? $data->jumlah: 0)}},-</td>
                                                <td style="vertical-align: middle;" align="right">Rp. {{number_format($data->tipe == 'keluar' ? $data->jumlah: 0)}},-</td>
                                                <td style="vertical-align: middle;" align="center">

                                                    <a href="{{url('/admin/keuangan/hapus').'/'.$data->id}}" onclick="if(confirm('Apakah Anda Yakin Akan Menghapus Posting Ini?')){return true;}else return false;" {{auth()->user()->can('hapus_postingan')? '' : 'disabled'}} title="Hapus Posting" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
                                                </td>
                                            </tr>
                                            <?php $i++; ?>
                                        @empty
                                            <tr>
                                                <td align="center" colspan="6">Tidak Ada Data Keuangan</td>
                                            </tr>
                                        @endforelse
                                        </tbody>
                                    </table>
                                </div>
                                <div class="text-center">
                                    {{$data_keuangan->links()}}
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('style')
    <link href="{{url('css/jquery.growl.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{url('css/bootstrap-datepicker3.min.css')}}" rel="stylesheet" type="text/css" />
    <style>
        span.month{
            color: #8d8d8d;
        }
        .konten {
            margin-top: 20px;
        }
        .konten > .row {
            margin-right: 20px;
            margin-left: 30px;
        }
        .row{
            margin-top: 10px;
            margin-bottom: 10px;
        }
        .form-group{
            margin-right: 20px;
        }
    </style>
@endsection
@section('script')
    <script src="{{url('js/jquery.growl.js')}}" type="text/javascript"></script>
    <script src="{{url('js/bootstrap-datepicker.min.js')}}" type="text/javascript"></script>

    <script>
                @if(request()->session()->has('sukses'))
        var msg = "{{request()->session()->get('sukses')}}";
        $.growl.notice({ title:"Sukses!", message: msg });
                @endif

                @if(request()->session()->has('gagal'))
        var msg = "{{request()->session()->get('gagal')}}";
        $.growl.error({ title:"Gagal!", message: msg });
                @endif
        var option = {
                clearBtn: true,
                autoclose: true,
                format: 'dd-mm-yyyy'
            };
        $('#dari_tanggal').datepicker(option);
        $('#sampai_tanggal').datepicker(option);
    </script>
@endsection