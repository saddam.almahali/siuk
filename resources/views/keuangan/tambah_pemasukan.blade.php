@extends('layouts.app')

@section('content')
    <div class="konten">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            Form Penerimaan Kas
                        </h3>
                    </div>
                    <div class="panel-body">
                        <form action="{{url('/admin/keuangan/simpan')}}" method="post" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <input type="hidden" name="id_anggota" value="{{auth()->user()->id}}">
                            <input type="hidden" name="tipe" value="masuk">
                            <div class="col-md-6">
                                <div class="form-group {{$errors->has('tanggal') ? 'has-error': ''}}">
                                    <input type="text" class="form-control" id='tanggal' name="tanggal" placeholder="Tanggal Terima" value="{{old('tanggal')}}">
                                    @if($errors->has('tanggal'))
                                        <span class="help-block">
                                                <strong>{{$errors->first('tanggal')}}</strong>
                                            </span>
                                    @endif
                                </div>
                                <div class="form-group {{$errors->has('jumlah') ? 'has-error': ''}}">
                                    <input type="number" class="form-control" name="jumlah" placeholder="Jumlah" value="{{old('jumlah')}}">
                                    @if($errors->has('jumlah'))
                                        <span class="help-block">
                                                <strong>{{$errors->first('jumlah')}}</strong>
                                            </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group {{$errors->has('keterangan') ? 'has-error': ''}}">
                                    <textarea name="keterangan" rows="1" class="form-control" placeholder="Keterangan Pemasukan"></textarea>
                                    @if($errors->has('keterangan'))
                                        <span class="help-block">
                                                <strong>{{$errors->first('keterangan')}}</strong>
                                            </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="submit" class="btn btn-primary btn-sm pull-right" value="Simpan">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>



    </div>
@endsection

@section('style')
    <link href="{{url('css/bootstrap-datepicker3.min.css')}}" rel="stylesheet" type="text/css" />
    <style>
        span.month{
            color: #8d8d8d;
        }
        .konten {
            margin-top: 20px;
        }
        .konten > .row {
            margin-right: 20px;
            margin-left: 30px;
        }
        .row{
            margin-top: 10px;
            margin-bottom: 10px;
        }
        .form-group{
            margin-right: 20px;
        }
    </style>
@endsection
@section('script')
    <script src="{{url('js/bootstrap-datepicker.min.js')}}" type="text/javascript"></script>
    <script>
                @if(request()->session()->has('sukses'))
        var msg = "{{request()->session()->get('sukses')}}";
        $.growl.notice({ title:"Sukses!", message: msg });
                @endif

                @if(request()->session()->has('gagal'))
        var msg = "{{request()->session()->get('gagal')}}";
        $.growl.error({ title:"Gagal!", message: msg });
                @endif
        var option = {
                clearBtn: true,
                autoclose: true,
                format: 'dd-mm-yyyy'
            };
        $('#tanggal').datepicker(option);
    </script>
@endsection