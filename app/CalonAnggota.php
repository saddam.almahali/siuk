<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CalonAnggota extends Model
{
    protected $table = 'calon_anggota';

    public function jenis()
    {
        return $this->belongsToMany('App\Model\JenisKegiatan', 'calon_jurusan_jenis', 'id_calon', 'id_jenis');
    }

}
