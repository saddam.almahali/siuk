<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CalonJurusanJenis extends Model
{
    protected $table = 'calon_jurusan_jenis';
    public $timestamps = false;
}
