<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Agenda extends Model
{
    protected $table = 'agenda';
    public function kegiatan()
    {
        return $this->hasOne('App\Model\JenisKegiatan', 'id', 'id_kegiatan');
    }
}
