<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Anggota extends Model
{
    protected $table = 'anggota';
    protected $fillable = [
        'npm', 'nama_lengkap', 'tanggal_daftar', 'jenis_kelamin', 'tempat_lahir',
        'tanggal_lahir', 'alamat_asal', 'alamat_kos'
    ];

    protected $hidden = [
        'created_at', 'updated_at'
    ];

    public function kegiatan()
    {
        return $this->belongsToMany('App\Model\JenisKegiatan','user_jenis_kegiatan', 'anggota_id', 'jenis_kegiatan_id');
    }



}
