<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class JenisKegiatan extends Model
{
    protected $table = 'jenis_kegiatan';

    public function anggota()
    {
        return $this->belongsToMany('App\Model\Anggota', 'user_jenis_kegiatan', 'jenis_kegiatan_id', 'anggota_id');
    }
}
