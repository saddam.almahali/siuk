<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Posting extends Model
{

    public function approve()
    {
        return $this->hasOne('App\Model\PostingApproval', 'id_posting','id');
    }

    public function gambar()
    {
        return $this->hasMany('App\Master\GambarPosting', 'id_posting', 'id');
    }
}
