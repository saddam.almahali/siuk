<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ProfileCompany extends Model
{
    protected $table = 'profile_company';
}
