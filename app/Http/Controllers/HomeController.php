<?php

namespace App\Http\Controllers;

use App\Model\Agenda;
use App\Model\Anggota;
use App\Model\DataKeuangan;
use App\Model\Posting;
use App\Model\PostingApproval;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(auth()->user()->hasRole('admin')){
            $data_anggota = Anggota::get()->count();

            $uang_masuk = DataKeuangan::where('tipe', '=', 'masuk')->sum('jumlah');
            $uang_keluar = DataKeuangan::where('tipe', '=', 'keluar')->sum('jumlah');
            $uang_kas = $uang_masuk-$uang_keluar;

            $jumlah_agenda = Agenda::where(DB::raw('tanggal::date'), '>=', date('Y-m-d'))->get()->count();
            $query = DB::table('posting_approval')->where('status', '=', 'approval')->select('id_posting');
            $jumlah_artikel = DB::table('postings')->whereIn('id',$query)->get()->count();

            $recent_anggota = new Anggota();
            $recent_anggota = $recent_anggota->orderBy('created_at', 'desc')->limit(5)->get();
//            echo json_encode($recent_anggota);
            return view('home', [
                'jumlah_anggota'=>$data_anggota,
                'uang_kas'=>$uang_kas,
                'jumlah_agenda'=>$jumlah_agenda,
                'jumlah_artikel'=>$jumlah_artikel,
                'recent_anggota'=>$recent_anggota
            ]);
        }else{
            return view('home');
        }

    }


}
