<?php

namespace App\Http\Controllers;

use App\Model\Agenda;
use App\Model\JenisKegiatan;
use Illuminate\Http\Request;

class AgendaController extends Controller
{
    public function index_agenda()
    {
        $data_agenda = new Agenda();
        $data_agenda = $data_agenda->paginate(10);
        return view('agenda.index', ['data_agenda'=>$data_agenda]);
    }

    public function tambah_agenda(Request $request)
    {
        if(auth()->user()->can('tambah_agenda'))
        {
            $data_kegiatan = JenisKegiatan::all();
            return view('agenda.baru', [
                'data_jenis'=>$data_kegiatan
            ]);
        }else{
            $request->session()->flash('gagal', 'Anda Tidak Memiliki Izin!');
            return redirect('/manage/agenda');
        }

    }

    public function simpan_agenda(Request $request)
    {
        $this->validate($request, [
            'jenis'=>'required',
            'nama_kegiatan'=>'required',
            'tanggal_waktu'=>'required',
            'agenda'=>'required',
            'sifat'=>'required',
        ]);
//        $date = date('H:i', strtotime($request->get('tanggal_waktu')));

//        echo json_encode($date);

        $agenda = new Agenda();
        $agenda->id_kegiatan = $request->get('jenis');
        $agenda->nama_kegiatan = $request->get('nama_kegiatan');
        $agenda->tanggal = date('Y-m-d', strtotime($request->get('tanggal_waktu')));
        $agenda->waktu = date('H:i', strtotime($request->get('tanggal_waktu')));
        $agenda->agenda = $request->get('agenda');
        $agenda->sifat = $request->get('sifat');
        $agenda->pembuat = auth()->user()->name;

        if($agenda->save())
        {
            $request->session()->flash('sukses', 'Berhasil Menambahkan Agenda');
            return redirect('/manage/agenda');
        }
    }

    public function detile_agenda($id_agenda, Request $request)
    {
        $agenda = Agenda::find($id_agenda);

        if($agenda)
        {
            return view('agenda.detile', [
                'agenda'=>$agenda
            ]);
        }else{
            $request->session()->flash('gagal', 'Agenda Tidak Ditemukan');
            return redirect('/manage/agenda');
        }

    }

    public function ubah_form($id_agenda, Request $request)
    {
        if(auth()->user()->can('ubah_agenda'))
        {
            $agenda = Agenda::find($id_agenda);
//            echo json_encode($agenda);
            $jenis = JenisKegiatan::all();
            if($agenda)
            {
                return view('agenda.ubah', [
                    'agenda'=>$agenda,
                    'data_jenis'=>$jenis
                ]);
            }else{
                $request->session()->flash('gagal', 'Agenda Tidak Ditemukan');
                return redirect('/manage/agenda');
            }

        }else{
            $request->session()->flash('gagal', 'Anda Tidak Mempunyai Izin Untuk Merubah Agenda');
            return redirect('/manage/agenda');
        }

    }

    public function simpan_perubahan(Request $request)
    {
        $this->validate($request, [
            'tanggal_waktu'=>'required'
        ]);

        $agenda = Agenda::find($request->get('id_agenda'));
        $agenda->tanggal = date('Y-m-d', strtotime($request->get('tanggal_waktu')));
        $agenda->waktu = date('H:i', strtotime($request->get('tanggal_waktu')));

        if($agenda->save())
        {
            $request->session()->flash('sukses', 'Berhasil Memutakhirkan Agenda');
            return redirect('/manage/agenda');
        }

    }

    public function hapus($id_agenda, Request $request)
    {
        if(auth()->user()->can('hapus_agenda'))
        {
            $agenda = Agenda::find($id_agenda);
            if($agenda)
            {
                if($agenda->delete()){
                    $request->session()->flash('sukses', 'Berhasil Menghapus Agenda');
                    return redirect('/manage/agenda');
                }
            }else{
                $request->session()->flash('gagal', 'Agenda Tidak Ditemukan');
                return redirect('/manage/agenda');
            }

        }else{
            $request->session()->flash('gagal', 'Anda Tidak Mempunyai Izin Untuk Menghapus Agenda');
            return redirect('/manage/agenda');
        }
    }
}
