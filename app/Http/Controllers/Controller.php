<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Storage;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    function getIdFile(Array $array, $key, $value)
    {
        foreach ($array as $subarray)
        {
            if (isset($subarray[$key]) && $subarray[$key] == $value)
                return $subarray['path'];
        }
    }

    public function getRealPathName($name_file)
    {
        $imagePath = $name_file;
        $ext = pathinfo($imagePath, PATHINFO_EXTENSION);

        $foto = basename($imagePath,".".$ext);

        return $foto;
    }

    public function hapus_file($nama_file)
    {
        $listContents = Storage::disk('google')->listContents();
        $id = $this->getIdFile($listContents, 'filename', $nama_file);
        Storage::disk('google')->delete($id);
        return true;
    }

}
