<?php

namespace App\Http\Controllers;

use App\CalonAnggota;
use App\CalonJurusanJenis;
use App\Model\JenisKegiatan;
use App\Model\Posting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class GuestController extends Controller
{

    public function home(){


        if(auth()->guest()){
            $data_agenda= \App\Model\Agenda::where(DB::raw('tanggal::date'), '>=', date('Y-m-d'))->limit(5)->get();
            $query = DB::table('posting_approval')->where('status', '=', 'approval')->select('id_posting');
            $data_artikel = Posting::whereIn('id',$query)->limit(3)->get();
            return view('welcome', [
                'data_agenda'=>$data_agenda,
                'data_posting'=>$data_artikel
            ]);
        }else{
            return redirect('/home');
        }
    }

    public function index_artikel()
    {
        $data_agenda= \App\Model\Agenda::where(DB::raw('tanggal::date'), '>=', date('Y-m-d'))->get();
        $query = DB::table('posting_approval')->where('status', '=', 'approval')->select('id_posting');
        $data_artikel = Posting::whereIn('id',$query)->paginate(6);
        return view('artikel.index', [
            'data_agenda'=>$data_agenda,
            'data_posting'=>$data_artikel
        ]);
    }

    public function daftar_anggota()
    {
        $jenis_kegiatan = JenisKegiatan::all();
        return view('daftar_anggota', [
            'data_jenis'=>$jenis_kegiatan
        ]);
    }

    public function registrasi_anggota(Request $request)
    {
        $this->validate($request, [
            'npm'=>'required',
            'nama_lengkap'=>'required',
            'jenis_kelamin'=>'required',
            'tempat_lahir'=>'required',
            'tanggal_lahir'=>'required',
            'email'=>'required',
            'jenis_kegiatan'=>'required',
        ]);
//        return json_encode($request->all());

        $anggota = new CalonAnggota();
        $anggota->npm = $request->input('npm');
        $anggota->nama_lengkap = $request->input('nama_lengkap');
        $anggota->tanggal_daftar = date('Y-m-d', strtotime($request->input('tanggal_daftar')));
        $anggota->tempat_lahir = $request->input('tempat_lahir');
        $anggota->tanggal_lahir = date('Y-m-d', strtotime($request->input('tanggal_lahir')));
        $anggota->jenis_kelamin = $request->input('jenis_kelamin');
        $anggota->email = $request->get('email');
        $anggota->alamat_asal= $request->input('alamat_asal');
        $anggota->alamat_kos= $request->input('alamat_kos');
        $anggota->alamat_asal= $request->input('alamat_asal');
        $anggota->telp = $request->input('telp');
        $anggota->facebook = $request->input('facebook');
        $anggota->twitter = $request->input('twitter');
        $anggota->bbm = $request->input('bbm');
        $anggota->hobi = $request->input('hobi');
        $anggota->moto = $request->input('moto');
        $anggota->harapan = $request->input('harapan');
        $imageName = '';
        if ($request->hasFile('foto')) {
            $imageName = str_random( 50).'.'.$request->file('foto')->getClientOriginalExtension();

//            echo $random;
//            $file_foto = $request->file('photo');
//            Storage::disk('google')->put($imageName, $file_foto);
            // $content = File::get($imageName);
//            $path = $request->photo->storeAs('images', $imageName, 'google');
            Storage::disk('google')->put($imageName, file_get_contents($request->file('foto')->getRealPath()));

        }

        $anggota->foto = $imageName;
        $anggota->status = 'bk';

        if($anggota->save()){
            $data_jenis = $request->get('jenis_kegiatan');
            if($data_jenis){
                foreach ($data_jenis as $jenis)
                {
                    $anggota->jenis()->attach($jenis);
                }
            }

            $request->session()->flash('sukses', 'Anda Berhasil Terdaftar!');
            return redirect('/daftar_anggota');


        }
    }

    public function list_berita(Request $request)
    {
        $data_agenda= \App\Model\Agenda::where(DB::raw('tanggal::date'), '>=', date('Y-m-d'))->limit(5)->get();
        $query = DB::table('posting_approval')->where('status', '=', 'approval')->select('id_posting');
        $data_artikel = Posting::whereIn('id',$query)->paginate(6);
        return view('list_berita', [
            'data_berita'=>$data_artikel,
            'data_agenda'=>$data_agenda
        ]);
    }

    public function detile_berita($id_berita, Request $request)
    {
        $data_agenda= \App\Model\Agenda::where(DB::raw('tanggal::date'), '>=', date('Y-m-d'))->limit(5)->get();
        $berita = Posting::find($id_berita);
        return view('single_post', [
            'posting'=>$berita,
            'data_agenda'=>$data_agenda
        ]);

    }

    public function list_agenda(){
        $data_agenda= \App\Model\Agenda::where(DB::raw('tanggal::date'), '>=', date('Y-m-d'))->limit(5)->get();

        return view('list_agenda', [
            'data_agenda'=>$data_agenda
        ]);
    }

    public function index_tentang(Request $request)
    {
        return view('index_tentang');
    }

}
