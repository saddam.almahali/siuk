<?php

namespace App\Http\Controllers\Master;

use App\Master\GambarPosting;
use App\Model\Anggota;
use App\Model\JenisKegiatan;
use App\Model\Posting;
use App\Model\PostingApproval;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class PostinganController extends Controller
{

    public $posting;

    public function __construct()
    {
        $this->posting = new Posting();
    }

    public function index_posting(Request $request)
    {

        $dari_tanggal = $request->get('dari_tanggal');
        $sampai_tanggal = $request->get('sampai_tanggal');
        $per_page = $request->get('per_page', 1);

        $data_posting = $this->createModel();

        if($dari_tanggal && $sampai_tanggal)
        {
            $dari_tanggal = date('Y-m-d', strtotime($dari_tanggal));
            $sampai_tanggal = date('Y-m-d', strtotime($sampai_tanggal));

            $data_posting = $data_posting->whereBetween('tanggal_posting', array($dari_tanggal, $sampai_tanggal));

            if($per_page)
            {
                $data_posting = $data_posting->paginate($per_page);
            }

            return view('postingan.index', [
                'data_posting'=>$data_posting
            ]);
        }else{
            $data_posting = $data_posting->paginate(5);

            return view('postingan.index', [
                'data_posting'=>$data_posting
            ]);
        }


    }

    public function posting_baru_form(Request $request)
    {
        if(auth()->user()->hasRole('admin')){
            $data_kegiatan = JenisKegiatan::all();

            return view('postingan.create', [
                'data_kegiatan'=>$data_kegiatan
            ]);
        }else{
            $anggota = Anggota::where('nama_lengkap', '=', auth()->user()->name)->first();
            $kegiatan = $anggota->kegiatan;
            $length_kegiatan = $anggota->kegiatan()->count();
            if($kegiatan){
                return view('postingan.create', [
                    'data_kegiatan'=>$kegiatan
                ]);
            }else{
                $request->session()->flash('gagal', 'Anda Belum Dimasukan Kedalam Anggota Kegiatan Apapun, Silahkan Menghubungi Admin Sistem');
                return redirect('/home');
            }

        }

    }

    public function simpan_posting(Request $request)
    {
        $this->validate($request, [
            'author'=>'required',
            'judul_posting'=>'required',
            'isi_posting'=>'required',
            'status'=>'required',
            'gambar'=>'required'
        ]);

        $newPost = new Posting();
        $newPost->author = $request->get('author');
        $newPost->judul_posting = $request->get('judul_posting');
        $newPost->isi_posting = $request->get('isi_posting');
        $newPost->created_by = auth()->user()->username;
        $newPost->tanggal_posting = date('Y-m-d');




        if($newPost->save())
        {
            if($request->hasFile('gambar'))
            {
                $imageName = str_random( 50).'.'.$request->file('gambar')->getClientOriginalExtension();
                Storage::disk('google')->put($imageName, file_get_contents($request->file('gambar')->getRealPath()));
                $newGambar = new GambarPosting();
                $newGambar->id_posting = $newPost->id;
                $newGambar->gambar = $imageName;

                $newGambar->save();
            }



            $aprove = new PostingApproval();
            $aprove->id_posting = $newPost->id;
            $aprove->status = $request->get('status');
            if($aprove->save()){
                $request->session()->flash('sukses', 'Berhasil Menambahkan Posting');
                return redirect('/posting');
            }

        }
    }

    public function hapus_posting($id_posting, Request $request)
    {
        if(auth()->user()->can('hapus_postingan'))
        {
            $posting = Posting::find($id_posting);

            if($posting != null)
            {
                $approve = $posting->approve;
                if($approve->delete())
                {
                    $gambar = $posting->gambar;
                    foreach ($gambar as $data)
                    {
                        $foto = $this->getRealPathName($data->gambar);
                        $listContents = Storage::disk('google')->listContents();
                        $id = $this->getIdFile($listContents, 'filename', $foto);
                        Storage::disk('google')->delete($id);

                        $data->delete();
                    }

                }

                if($posting->delete())
                {
                    $request->session()->flash('sukses', 'Berhasil Menghapus Postingan!');
                    return redirect('/posting');
                }
            }
        }else{
            $request->session()->flash('gagal', 'Anda Tidak Mempunyai Izin Untuk Menghapus Artikel');
            return redirect('/posting');
        }

    }

    public function edit_posting($id_posting, Request $request)
    {
        if(auth()->user()->can('edit_postingan')){
            $posting = Posting::find($id_posting);

            if($posting != null)
            {
                return view('postingan.edit', [
                    'posting'=>$posting
                ]);
            }
        }else{
            $request->session()->flash('gagal', 'Anda Tidak Mempunyai Izin Untuk Menghapus Artikel');
            return redirect('/posting');
        }

    }

    public function simpan_perubahan(Request $request)
    {
        $this->validate($request, [
            'author'=>'required',
            'judul_posting'=>'required',
            'isi_posting'=>'required',
            'status'=>'required'
        ]);

        $newPost = Posting::find($request->get('id_posting'));
//        $newPost->author = $request->get('author');
        $newPost->judul_posting = $request->get('judul_posting');
        $newPost->isi_posting = $request->get('isi_posting');
        $newPost->tanggal_posting = date('Y-m-d');




        if($newPost->save())
        {
            if($request->hasFile('gambar'))
            {

                $imageName = str_random( 50).'.'.$request->file('gambar')->getClientOriginalExtension();


                $newGambar = $newPost->gambar()->first();
                $newGambar->id_posting = $newPost->id;
                //Hapus Gambar Lama
                $gambarLama = $this->getRealPathName($newGambar->gambar);

                if($this->hapus_file($gambarLama)){
                    Storage::disk('google')->put($imageName, file_get_contents($request->file('gambar')->getRealPath()));
                }

                $newGambar->gambar = $imageName;

                $newGambar->save();
            }


            if($request->get('status') != $newPost->approve->status){
                $aprove = $newPost->approve;
                $aprove->id_posting = $newPost->id;
                $aprove->status = $request->get('status');
                $aprove->save();
            }


            $request->session()->flash('sukses', 'Berhasil Memutakhirkan Posting!');
            return redirect('/posting');


        }
    }

    public function preview_posting($id_posting, Request $request)
    {

        $posting = Posting::find($id_posting);
        if($posting->auhtor != 'admin'){
            $anggota = Anggota::where('nama_lengkap', '=', $posting->author)->first();
            return view('postingan.preview', [
                'posting'=>$posting,
                'anggota'=>$anggota
            ]);
        }else{
            return view('postingan.preview', [
                'posting'=>$posting
            ]);
        }

    }

    public function createModel()
    {
        return new $this->posting;
    }


}
