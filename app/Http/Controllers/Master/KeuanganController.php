<?php

namespace App\Http\Controllers\Master;

use App\Model\DanaKeluar;
use App\Model\DanaMasuk;
use App\Model\DataKeuangan;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class KeuanganController extends Controller
{
    public function index_keuangan()
    {
        $data_keuangan = new DataKeuangan();
        $data_keuangan = $data_keuangan->paginate(5);
        $jumlah_pengeluaran = $data_keuangan->where('tipe', '=', 'keluar')->sum('jumlah');
        $jumlah_pemasukan = $data_keuangan->where('tipe', '=', 'masuk')->sum('jumlah');

        if(empty($jumlah_pemasukan)){
            $jumlah_pemasukan = 0;
        }

        if(empty($jumlah_pengeluaran)){
            $jumlah_pengeluaran = 0;
        }

        $uang_kas = $jumlah_pemasukan-$jumlah_pengeluaran;

        return view('keuangan.index', [
            'data_keuangan'=>$data_keuangan,
            'uang_kas'=>$uang_kas
        ]);
    }

    public function tambah_pemasukan()
    {
        return view('keuangan.tambah_pemasukan');
    }

    public function tambah_pengeluaran()
    {
        return view('keuangan.tambah_pengeluaran');
    }

    public function simpan(Request $request)
    {
        $this->validate($request, [
            'tanggal'=>'required',
            'jumlah'=>'required',
            'keterangan'=>'required'
        ], [
            'tanggal.required'=>'Kolom Tanggal Tidak Boleh Kosong',
            'jumlah.required'=>'Kolom Jumlah Tidak Boleh Kosong',
            'keterangan.required'=>'Kolom Keterangan Tidak Boleh Kosong'
        ]);

        $tipe = $request->get('tipe');
        $data_keuangan = new DataKeuangan();

        $data_keuangan->id_anggota = $request->get('id_anggota');
        $data_keuangan->tanggal = date('Y-m-d', strtotime($request->get('tanggal')));
        $data_keuangan->jumlah= $request->get('jumlah');
        $data_keuangan->keterangan= $request->get('keterangan');
        $total = 0;
        $data_last = DataKeuangan::orderBy('created_at', 'desc')->first();
        if($tipe == 'masuk'){
            if($data_last){
                $total = $data_last->total+$request->get('jumlah');
            }else{
                $total = 0+$request->get('jumlah');
            }
        }else{
            if($data_last){
                $total = $data_last->total-$request->get('jumlah');
            }else{
                $total = 0-$request->get('jumlah');
            }
        }

        $data_keuangan->total = $total;
        $data_keuangan->tipe = $tipe;

        if($data_keuangan->save())
        {
            $request->session()->flash('sukses', 'Data Keuangan Berhasil Ditambahkan');
            return redirect('/admin/keuangan');
        }
    }

    public function index_laporan()
    {
        $dari_tanggal = request()->get('dari_tanggal');
        $sampai_tanggal = request()->get('sampai_tanggal');
        if(empty($dari_tanggal) && empty($sampai_tanggal)){
            return view('laporan_keuangan.index');
        }else{
            $data_keuangan = DataKeuangan::whereBetween('tanggal', [date('Y-m-d', strtotime($dari_tanggal)), date('Y-m-d', strtotime($sampai_tanggal))])->get();
//            echo json_encode($data_keuangan);
            return view('laporan_keuangan.hasil', [
                'dari_tanggal'=>$dari_tanggal,
                'sampai_tanggal'=>$sampai_tanggal,
                'data_keuangan'=>$data_keuangan
            ]);
        }

    }

    public function hapus_keuangan($id_keuangan, Request $request)
    {
        $data_keuangan = DataKeuangan::find($id_keuangan);
        if($data_keuangan){
            if($data_keuangan->delete()){
                $request->session()->flash('sukses', 'Data Keuangan Berhasil Dihapus!');
                return redirect('/admin/keuangan');
            }
        }else{
            $request->session()->flash('gagal', 'Data Keuangan Tidak Ditemukan!');
            return redirect('/admin/keuangan');
        }
    }
}
