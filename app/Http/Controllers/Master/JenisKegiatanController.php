<?php

namespace App\Http\Controllers\Master;

use App\Model\Anggota;
use App\Model\JenisKegiatan;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;



class JenisKegiatanController extends Controller
{
    public function index()
    {
        $data_jenis = new JenisKegiatan();
        $data_jenis = $data_jenis->paginate(5);
        return view('master.jenis_kegiatan.index', [
            'data_jenis'=>$data_jenis
        ]);
    }

    public function tambah_form()
    {
        return view('master.jenis_kegiatan.tambah');
    }

    public function simpan_jenis(Request $request)
    {
        $this->validate($request, [
            'kode_jenis'=>'required|max:6|min:2',
            'nama_jenis'=>'required|min:3|max:20',
            'keterangan'=>'required|min:4|max:255'
        ]);

        $jenis_kegiatan = new JenisKegiatan();
        $jenis_kegiatan->kode_jenis = $request->get('kode_jenis');
        $jenis_kegiatan->nama_jenis = $request->get('nama_jenis');
        $jenis_kegiatan->keterangan = $request->get('keterangan');

        if($jenis_kegiatan->save())
        {
            $request->session()->flash('sukses', 'Berhasil Menambahkan data ke database!');
            return redirect('/admin/jenis_kegiatan');
        }
    }

    public function update_form($id_jenis, Request $request)
    {
        $jenis_kegiatan = JenisKegiatan::find($id_jenis);
        if($jenis_kegiatan)
        {
            return view('master.jenis_kegiatan.update', [
                'jenis_kegiatan'=>$jenis_kegiatan
            ]);
        }else{
            $request->session()->flash('gagal', 'Data Tidak Ditemukan!');
            return redirect('/admin/jenis_kegiatan');
        }
    }

    public function update_jenis(Request $request)
    {
        $this->validate($request, [
            'kode_jenis'=>'required|max:6|min:2',
            'nama_jenis'=>'required|min:3|max:20',
            'keterangan'=>'required|min:4|max:255'
        ]);

        $jenis_kegiatan = JenisKegiatan::find($request->id_jenis);
        $jenis_kegiatan->kode_jenis = $request->get('kode_jenis');
        $jenis_kegiatan->nama_jenis = $request->get('nama_jenis');
        $jenis_kegiatan->keterangan = $request->get('keterangan');

        if($jenis_kegiatan->save())
        {
            $request->session()->flash('sukses', 'Berhasil Memutakhirkan data ke database!');
            return redirect('/admin/jenis_kegiatan');
        }
    }

    public function hapus_jenis($id_jenis, Request $request)
    {
        $jenis = JenisKegiatan::find($id_jenis);
        if($jenis)
        {
            if($jenis->delete()){
                $request->session()->flash('sukses', 'Berhasil Menghapus Data Dari Database!');
                return redirect('/admin/jenis_kegiatan');
            }
        }else{
            $request->session()->flash('gagal', 'Data Tidak Ditemukan!');
            return redirect('/admin/jenis_kegiatan');
        }
    }

    public function view_jenis_kegiatan($id_jenis, Request $request)
    {
        $jenis_kegiatan = JenisKegiatan::find($id_jenis);
        $data_anggota_jenis = DB::table('user_jenis_kegiatan')->where('jenis_kegiatan_id','=', $jenis_kegiatan->id)->select('anggota_id');
        $data_anggota = Anggota::whereIn('id', $data_anggota_jenis)->get();
        return view('master.jenis_kegiatan.view',
            compact(['jenis_kegiatan', 'data_anggota'])
        );
    }

    public function tambah_anggota($jenis_kegiatan_id, Request $request)
    {

        $jenis_kegiatan = JenisKegiatan::find($jenis_kegiatan_id);
        $data_anggota_jenis = DB::table('user_jenis_kegiatan')->where('jenis_kegiatan_id','=', $jenis_kegiatan->id)->select('anggota_id');
        $data_anggota = Anggota::whereNotIn('id', $data_anggota_jenis)->get();
        return view('master.jenis_kegiatan.tambah_anggota', [
            'jenis_kegiatan'=>$jenis_kegiatan,
            'data_anggota'=>$data_anggota
        ]);
    }

    public function simpan_anggota(Request $request)
    {
        $this->validate($request, [
            'anggota'=>'required',
            'id_jenis'=>'required'
        ]);


        $id_jenis = $request->get('id_jenis');

        $id_anggota = $request->get('anggota');
        $anggota = Anggota::find($id_anggota);
        $jenis = JenisKegiatan::find($id_jenis);


        $jenis->anggota()->attach($id_anggota);
        $request->session()->flash('sukses', 'Berhasil Menambahkan Anggota!');
        return redirect('/admin/jenis_kegiatan/view/'.$id_jenis);


    }

    public function unlink_anggota($id_jenis, $id_anggota, Request $request)
    {
        $jenis = JenisKegiatan::find($id_jenis);
        $anggota = Anggota::find($id_anggota);

        $jenis->anggota()->detach($id_anggota);
        $request->session()->flash('sukses', 'Berhasil Mengeluarkan Anggota Dari Kegiatan!');
        return redirect('/admin/jenis_kegiatan/view/'.$id_jenis);
    }


}
