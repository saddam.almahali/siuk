<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class FileManagerController extends Controller
{
    public function get_foto($id)
    {
        $foto = $this->getRealPathName($id);
        $listContents = Storage::disk('google')->listContents();
        $id = $this->getIdFile($listContents, 'filename', $foto);
//        echo $id['path'];
        header("Content-type: image/jpeg");
        echo Storage::disk('google')->get($id);

    }
}
