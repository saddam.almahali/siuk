<?php

namespace App\Http\Controllers\Master;

use App\Permission;
use App\Role;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PreferencesController extends Controller
{
    public function index()
    {
        $role = Role::all();
        return view('master.preferences.index', [
            'data_role'=> $role
        ]);
    }

    public function index_role()
    {
        $role = Role::all();
        return view('master.preferences.role.index', [
            'data_role'=>$role
        ]);
    }

    public function tambah_role_form()
    {
        return view('master.preferences.role.tambah');
    }

    public function simpan_role(Request $request)
    {
        $this->validate($request, [
            'name'=> 'required|min:3',
            'display_name'=>'required|min:3',
            'description'=> 'required'
        ]);

        $role = new Role();
        $role->name = $request->get('name');
        $role->display_name = $request->get('display_name');
        $role->description = $request->get('description');

        if($role->save())
        {
            $request->session()->flash('sukses', 'Sukses Menambahkan data kedalam database!');
            return redirect('/admin/preferences/role');
        }
    }

    public function hapus_role($id_role, Request $request)
    {
        $data_role = Role::whereId($id_role);
        if($data_role)
        {
            // Force Delete
//            $data_role->users()->sync([]); // Delete relationship data
//            $data_role->perms()->sync([]); // Delete relationship data
            if($data_role->delete())
            {
                $request->session()->flash('sukses', 'Data Berhasil Dihapus dari Database!');
                return redirect('/admin/preferences/role');
            }

        }else{
            $request->session()->flash('gagal', 'Data Tidak Ditemukan!');
            return redirect('/admin/preferences/role');
        }
    }

    public function index_hakakses()
    {
        $data_role = Role::all();

        return view('master.preferences.hakakses.index',[
            'data_role'=>$data_role
        ]);
    }

    public function form_tambah_hakakses()
    {
        $data_role = Role::all();
        return view('master.preferences.hakakses.tambah', [
            'data_role'=>$data_role
        ]);
    }


    public function simpan_hakakses(Request $request)
    {
        $this->validate($request, [
            'id_role'=> 'required',
            'name'=> 'required|min:3',
            'display_name'=>'required|min:3',
            'description'=> 'required'
        ]);
        $role = Role::find($request->input('id_role'));

        $newPermission = new Permission();
        $newPermission->name = $request->get('name');
        $newPermission->display_name = $request->get('display_name');
        $newPermission->description = $request->get('');

        if($newPermission->save())
        {
            if($role){
                $role->attachPermission($newPermission);

            }

            $request->session()->flash('sukses', 'Sukses Menambahkan Data Kedalam Database!');
            return redirect('/admin/preferences/hakakses');
        }else{
            $request->session()->flash('gagal', 'Gagal Menambahkan Data Kedalam Database!');
            return redirect('/admin/preferences/hakakses');
        }
//
//        $role = new Role();
//        $role->name = $request->get('name');
//        $role->display_name = $request->get('display_name');
//        $role->description = $request->get('description');
//
//        if($role->save())
//        {
//            $request->session()->flash('sukses', 'Sukses Menambahkan data kedalam database!');
//            return redirect('/admin/preferences/role');
//        }
    }

    public function hapus_permission($id_role, $id_permission, Request $request)
    {
        $role = Role::whereId($id_role)->first();
        $permission = Permission::whereId($id_permission)->first();
        $role->detachPermission($permission);
        if ($permission->delete()) {
            $request->session()->flash('sukses', 'Sukses Menghapus Permission!');
            return redirect('/admin/preferences/hakakses');
        }


    }

    public function set_permission_form($id_role, Request $request)
    {
        $role = Role::whereId($id_role)->first();
        $permission = Permission::all();
        return view('master.preferences.hakakses.set_permission', [
            'role'=>$role,
            'data_permission'=>$permission
        ]);
    }

    public function set_permission(Request $request)
    {
        $this->validate($request, [
            'id_permission'=>'required'
        ]);

        $role = Role::whereId($request->get('id_role'))->first();

        $permission = Permission::whereId($request->get('id_permission'))->first();

        $role->attachPermission($permission);

        $request->session()->flash('sukses', 'Sukses menambahkan Permission kedalam Role!');
        return redirect('/admin/preferences/hakakses');
    }

    public function unlink_permission($id_role, $id_permission, Request $request)
    {
        $role = Role::whereId($id_role)->first();
        $permission = Permission::whereId($id_permission)->first();
        $role->detachPermission($permission);
            $request->session()->flash('sukses', 'Sukses Menghapus Akses Permission!');
            return redirect('/admin/preferences/hakakses');


    }

    public function index_usermanajemen()
    {
        $data_user = new User();
        $data_user = $data_user->paginate(10);
        return view('master.preferences.usermanajemen.index', [
            'data_user'=>$data_user
        ]);
    }


}
