<?php

namespace App\Http\Controllers\Master;

use App\Model\JenisKegiatan;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PenempatanAnggotaController extends Controller
{
    public function index()
    {
        $jenis_kegiatan = JenisKegiatan::all();
        return view('master.penempatan.index', [
            'jenis_kegiatan'=>$jenis_kegiatan
        ]);
    }

    public function get_data_jenis()
    {
        $jenis = JenisKegiatan::get()->toArray();
        return $jenis;
    }

}
