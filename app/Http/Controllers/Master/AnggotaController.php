<?php

namespace App\Http\Controllers\Master;

use App\CalonAnggota;
use App\Role;
use App\User;
use Faker\Provider\Image;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Anggota;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class AnggotaController extends Controller
{
    public function index()
    {
        $data_anggota = new Anggota();
        $data_anggota = $data_anggota->paginate(10);
        return view('master.anggota.index', [
            'data_anggota'=>$data_anggota
        ]);
    }

    public function form_tambah()
    {
        return view('master.anggota.tambah');
    }

    public function simpan_anggota(Request $request)
    {
        $this->validate($request, [
            'npm'=>'required',
            'nama_lengkap'=> 'required|max:255',
            'tanggal_daftar'=> 'required|date',
            'tempat_lahir'=> 'required|max:50',
            'email'=>'required|email',
            'tanggal_lahir'=> 'required|date',
            'jenis_kelamin'=> 'required',
            'alamat_asal'=> 'required',
            'alamat_kos'=>'required'
        ]);

        $anggota = new Anggota();
        $anggota->npm = $request->input('npm');
        $anggota->nama_lengkap = $request->input('nama_lengkap');
        $anggota->tanggal_daftar = date('Y-m-d', strtotime($request->input('tanggal_daftar')));
        $anggota->tempat_lahir = $request->input('tempat_lahir');
        $anggota->tanggal_lahir = date('Y-m-d', strtotime($request->input('tanggal_lahir')));
        $anggota->jenis_kelamin = $request->input('jenis_kelamin');
//        $anggota->email = $request->get('email');
        $anggota->alamat_asal= $request->input('alamat_asal');
        $anggota->alamat_kos= $request->input('alamat_kos');
        $anggota->alamat_asal= $request->input('alamat_asal');
        $anggota->telp = $request->input('telp');
        $anggota->facebook = $request->input('facebook');
        $anggota->twitter = $request->input('twitter');
        $anggota->bbm = $request->input('bbm');
        $anggota->hobi = $request->input('hobi');
        $anggota->moto = $request->input('moto');
        $anggota->harapan = $request->input('harapan');
        $imageName = '';
        if ($request->hasFile('photo')) {
            $imageName = str_random( 50).'.'.$request->file('photo')->getClientOriginalExtension();

//            echo $random;
//            $file_foto = $request->file('photo');
//            Storage::disk('google')->put($imageName, $file_foto);
           // $content = File::get($imageName);
//            $path = $request->photo->storeAs('images', $imageName, 'google');
            Storage::disk('google')->put($imageName, file_get_contents($request->file('photo')->getRealPath()));

        }

        $anggota->foto = $imageName;
        $anggota->status = true;

        if($anggota->save()){
           $user = new User();
           $user->name = $anggota->nama_lengkap;
           $user->email = $request->email;
           $user->username = $anggota->npm;
           $user->password = bcrypt(date('dmY', strtotime($anggota->tanggal_lahir)));
           if($user->save())
           {
               $role = Role::where('name', '=', 'user')->first();
               if($role)
               {
                   $user->attachRole($role);
               }
               $request->session()->flash('sukses', 'Data Berhasil Disimpan!');
               return redirect('/admin/anggota');
           }

        }
    }

    public function get_foto($id)
    {
        $anggota = Anggota::find($id);
        $foto = $this->getRealPathName($anggota->foto);
        $listContents = Storage::disk('google')->listContents();
        $id = $this->getIdFile($listContents, 'filename', $foto);
//        echo $id['path'];
        header("Content-type: image/jpeg");
        echo Storage::disk('google')->get($id);

    }

    public function view_anggota($id_anggota)
    {
        $anggota = Anggota::find($id_anggota);
        if($anggota != null)
        {
            return view('master.anggota.view', [
                'anggota'=>$anggota
            ]);
        }
    }

    public function konfirmasi_form($id_anggota, Request $request)
    {
        $anggota = Anggota::find($id_anggota);

        return view('master.anggota.konfirmasi', [
            'anggota'=> $anggota
        ]);
    }

    public function konfirmasi_anggota($id_anggota, Request $request)
    {
        $anggota = Anggota::find($id_anggota);
        if($anggota != null)
        {
            $user = new User();
            $user->name = $anggota->nama_lengkap;
            $user->username = $anggota->npm;
            $user->email = $anggota->npm.'@sttgarut.ac.id';
            $user->password = bcrypt(date('dmY', strtotime($anggota->tanggal_lahir)));
            $user->save();
        }else {
            $request->session()->flash('gagal', 'User Tidak Ditemukan!');
        }
    }

    public function hapus_anggota($id_anggota, Request $request)
    {
        $anggota = Anggota::findOrFail($id_anggota);
        if($anggota != null){
            $foto = $this->getRealPathName($anggota->foto);
            $listContents = Storage::disk('google')->listContents();
            $id = $this->getIdFile($listContents, 'filename', $foto);
            Storage::disk('google')->delete($id);

            $user = User::where('username', '=', $anggota->npm)->first();

            if($anggota->delete())
            {
                $request->session()->flash('sukses', 'Data Berhasil Dihapus dari Database!');
                return redirect('/admin/anggota');
            }

        }else{
            $request->session()->flash('gagal', 'Data Tidak Ditemukan!');
            return redirect('/admin/anggota');
        }

    }

    public function update_form($id_anggota, Request $request)
    {
        $anggota = Anggota::find($id_anggota);
        if($anggota != null)
        {
            return view('master.anggota.update', [
                'anggota'=> $anggota
            ]);
        }else{
            $request->session()->flash('gagal', 'Data Tidak Ditemukan!');
            return redirect('/admin/anggota');
        }
    }

    public function update_anggota(Request $request)
    {

        $this->validate($request, [
            'npm'=>'required',
            'nama_lengkap'=> 'required|max:255',
            'tanggal_daftar'=> 'required|date',
            'tempat_lahir'=> 'required|max:50',
            'tanggal_lahir'=> 'required|date',
            'jenis_kelamin'=> 'required',
            'alamat_asal'=> 'required',
            'alamat_kos'=>'required'
        ]);

        $anggota = Anggota::findOrFail($request->get('id_anggota'));
        $anggota->npm = $request->get('npm');
        $anggota->nama_lengkap = $request->get('nama_lengkap');
        $anggota->tanggal_daftar = date('Y-m-d', strtotime($request->get('tanggal_daftar')));
        $anggota->tempat_lahir = $request->get('tempat_lahir');
        $anggota->tanggal_lahir = date('Y-m-d', strtotime($request->get('tanggal_lahir')));
        $anggota->jenis_kelamin = $request->get('jenis_kelamin');
        $anggota->alamat_asal= $request->get('alamat_asal');
        $anggota->alamat_kos= $request->get('alamat_kos');
        $anggota->alamat_asal= $request->get('alamat_asal');
        $anggota->telp = $request->get('telp');
        $anggota->facebook = $request->get('facebook');
        $anggota->twitter = $request->get('twitter');
        $anggota->bbm = $request->get('bbm');
        $anggota->hobi = $request->get('hobi');
        $anggota->moto = $request->get('moto');
        $anggota->harapan = $request->get('harapan');
        $imageName = '';
        if ($request->hasFile('photo')) {
            $imageName = str_random( 50).'.'.$request->file('photo')->getClientOriginalExtension();

//            echo $random;
//            $file_foto = $request->file('photo');
//            Storage::disk('google')->put($imageName, $file_foto);
            // $content = File::get($imageName);
//            $path = $request->photo->storeAs('images', $imageName, 'google');
            Storage::disk('google')->put($imageName, file_get_contents($request->file('photo')->getRealPath()));

        }

        if($imageName)
        {
            $anggota->foto = $imageName;
        }
        $anggota->status = true;

        if($anggota->save()){
            $request->session()->flash('sukses', 'Data Berhasil Dimutakhirkan!');
            return redirect('/admin/anggota');
        }else{
            $request->session()->flash('gagal', 'Data Gagal Dimutakhirkan!');
            return redirect('/admin/anggota');
        }
    }

    public function index_pendaftaran()
    {
        $data_anggota = CalonAnggota::where('status', '=', 'bk');
        $data_anggota = $data_anggota->paginate(10);
        return view('master.pendaftaran.index', [
            'data_anggota'=>$data_anggota
        ]);
    }

    public function detile_calon($id_calon, Request $request)
    {
        $anggota = CalonAnggota::find($id_calon);
        if($anggota != null)
        {
            return view('master.pendaftaran.view', [
                'anggota'=>$anggota
            ]);
        }
    }

    public function get_foto_calon($id_calon, Request $request)
    {
        $anggota = CalonAnggota::find($id_calon);
        $foto = $this->getRealPathName($anggota->foto);
        $listContents = Storage::disk('google')->listContents();
        $id = $this->getIdFile($listContents, 'filename', $foto);
//        echo $id['path'];
        header("Content-type: image/jpeg");
        echo Storage::disk('google')->get($id);
    }

    public function konfirmasi_calon($id_calon, Request $request)
    {
        $calon = CalonAnggota::find($id_calon);
        if($calon){
            $check = Anggota::where('npm', '=', $calon->npm)->first();
            if(!$check){
                $anggota = new Anggota();
                $anggota->npm = $calon->npm;
                $anggota->nama_lengkap = $calon->nama_lengkap;
                $anggota->tanggal_daftar = date('Y-m-d', strtotime($calon->tanggal_daftar));
                $anggota->tempat_lahir = $calon->tempat_lahir;
                $anggota->tanggal_lahir = date('Y-m-d', strtotime($calon->tanggal_lahir));
                $anggota->jenis_kelamin = $calon->jenis_kelamin;
                $anggota->alamat_asal= $calon->alamat_asal;
                $anggota->alamat_kos= $calon->alamat_kos;
                $anggota->telp = $calon->telp;
                $anggota->facebook = $calon->facebook;
                $anggota->twitter = $calon->twitter;
                $anggota->bbm = $calon->bbm;
                $anggota->hobi = $calon->hobi;
                $anggota->moto = $calon->moto;
                $anggota->harapan = $calon->harapan;
                $anggota->foto = $calon->foto;
                $anggota->status = true;

                if($anggota->save()){
                    if($calon->jenis){
                        foreach ($calon->jenis as $jenis){
                            $anggota->kegiatan()->attach($jenis->id);
                        }
                    }
                    $user = new User();
                    $user->name = $anggota->nama_lengkap;
                    $user->email = $calon->email;
                    $user->username = $anggota->npm;
                    $user->password = bcrypt(date('dmY', strtotime($anggota->tanggal_lahir)));
                    if($user->save())
                    {
                        $role = Role::where('name', '=', 'user')->first();
                        if($role)
                        {
                            $user->attachRole($role);
                        }
                        $calon->status = 'k';
                        $calon->save();
                        $request->session()->flash('sukses', 'Data Berhasil Disimpan!');
                        return redirect('/admin/pendaftaran');
                    }

                }
            }else{
                $request->session()->flash('gagal', 'Data Calon Sudah Terdaftar!');
                return redirect('/admin/pendaftaran');

            }
        }else{
            $request->session()->flash('gagal', 'Data Calon Tidak Ditemukan!');
            return redirect('/admin/pendaftaran');
        }
    }

}
